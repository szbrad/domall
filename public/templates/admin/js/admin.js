/**
 * Created by Doogie on 2016/3/9.
 */

$('document').ready(function() {
    $.extend({
        getParams: function() {
            var aQuery = window.location.href.split("?");
            var aGet = new Array();
            if (aQuery.length > 1)
            {
                var aParam = aQuery[1].split("&");
                for (var i = 0, aParamLength = aParam.length; i < aParamLength; i++)
                {
                    var aTemp = aParam[i].split("=");
                    aGet[aTemp[0]] = aTemp[1];
                }
            }
            return aGet;
        },
        isEmpty: function(obj) {
            for (var name in obj) {
                return false;
            }
            return true;
        },
        timeChange: function(source, inFormat, outFormat) {
            var checkTime = function(time) {
                if(time < 10) {
                    time = "0" + time;
                };
                return time;
            };
            switch(inFormat) {
                case 'Y-m-d H:i:s':
                    var reg =  /^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2}) (\d{1,2}):(\d{1,2}):(\d{1,2})$/;
                    source = source.match(reg);
                    source = new Date(source[1], source[3]-1, source[4], source[5], source[6], source[7]);
                    break;
                case 'Y-m-d':
                    var reg = /^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2})$/;
                    source = source.match(reg);
                    source = new Date(source[1], source[3]-1, source[4]);
                    break;
                case 'timestamp':
                    source = new Date(parseInt(source)*1000);
                    break;
            };
            switch(outFormat) {
                case 'Y-m-d H:i:s':
                    return source.getFullYear() + '-'
                        + checkTime(source.getMonth()+1)
                        + '-'
                        + checkTime(source.getDate())
                        + ' '
                        + checkTime(source.getHours())
                        + ':'
                        + checkTime(source.getMinutes())
                        + ':'
                        + checkTime(source.getSeconds());
                    break;
                case 'Y-m-d':
                    return source.getFullYear() + '-'
                        + checkTime(source.getMonth()+1)
                        + '-'
                        + checkTime(source.getDate());
                    break;
                case 'Y.m.d':
                    return source.getFullYear() + '.'
                        + checkTime(source.getMonth()+1)
                        + '.'
                        + checkTime(source.getDate());
                    break;
                case 'timestamp':
                    return parseInt(source.getTime()/1000);
                    break;
                case 'newDate':
                    return source;
                    break;
            };
        }
    });

    //$('.' + bodyId).addClass('active');

    //点击backBtn按钮返回
    $('.domall-back').on('click', function(){
        window.history.go(-1);
    });

    //全选/反选/单选的实现
    $(".check-all").click(function() {
        $(".ids").prop("checked", this.checked);
    });

    $(".ids").click(function() {
        var option = $(".ids");
        option.each(function() {
            if (!this.checked) {
                $(".check-all").prop("checked", false);
                return false;
            } else {
                $(".check-all").prop("checked", true);
            }
        });
    });

    /**
     * 设置数据状态
     */
    $('.ajax-set-status').on('click', function(){
        var data = {ids: $(this).attr('__data_id__'), status: $(this).attr('__status__')};
        var url = $(this).attr('__href__');
        if(data.status == 'delete'){
            if(!confirm('确定删除此条数据吗？')){
                return;
            }
        }
        $.get(url, data,function(info){
            if(info.code = 1){
                alert(info.msg);
            }else{
                alert(info.msg);
            }
            location.reload();
        }, 'json');
    });
    /**
     * 批量设置数据状态
     */
    $('.set-status-batch').on('click', function(){
        var t = $('.ids');
        var data = t.serialize() +'&status='+$(this).attr('__status__');
        var url = $(this).attr('__href__');
        if(!confirm('确定操作吗？')){
            return;
        }
        $.get(url, data,function(info){
            if(info.code = 1){
                alert(info.msg);
            }else{
                alert(info.msg);
            }
            location.reload();
        }, 'json');
    });

});

