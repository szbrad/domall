-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        5.7.9 - MySQL Community Server (GPL)
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- 导出 domall 的数据库结构
CREATE DATABASE IF NOT EXISTS `domall` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `domall`;

-- 导出  表 domall.do_activity 结构
CREATE TABLE IF NOT EXISTS `do_activity` (
  `activity_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `item_id` int(11) NOT NULL COMMENT '商品编号',
  `item_name` varchar(255) NOT NULL COMMENT '商品名称',
  `activity_code` varchar(255) NOT NULL COMMENT '当前期号',
  `activity_type` int(10) DEFAULT NULL COMMENT '活动类型 1:商品 2:团购 3:一元一拍',
  `activity_max_num` int(10) DEFAULT '0' COMMENT '活动次数，一元一拍最大期数',
  `activity_sort` tinyint(1) unsigned NOT NULL DEFAULT '255' COMMENT '排序',
  `activity_addtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  `activity_edittime` int(10) unsigned NOT NULL COMMENT '编辑时间',
  PRIMARY KEY (`activity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='活动表';

-- 正在导出表  domall.do_activity 的数据：~5 rows (大约)
DELETE FROM `do_activity`;
/*!40000 ALTER TABLE `do_activity` DISABLE KEYS */;
INSERT INTO `do_activity` (`activity_id`, `item_id`, `item_name`, `activity_code`, `activity_type`, `activity_max_num`, `activity_sort`, `activity_addtime`, `activity_edittime`) VALUES
	(1, 100000, '周生生 18K金 白色黄金钻戒Belief女款 指寸12', '1', 3, 99, 0, 1462200293, 1462200293),
	(2, 100001, 'd', '1', 3, 0, 0, 1462323846, 1462323846),
	(3, 100002, 'e', '1', 3, 98, 0, 1462341024, 1462341024),
	(4, 100003, '分的纪录时刻规划', '1', 3, 321, 0, 1462756672, 1462756672),
	(5, 100004, '有人提议天天', '1', 3, 534, 0, 1462756733, 1462756733);
/*!40000 ALTER TABLE `do_activity` ENABLE KEYS */;

-- 导出  表 domall.do_activity_detail 结构
CREATE TABLE IF NOT EXISTS `do_activity_detail` (
  `activity_detail_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `activity_id` int(10) unsigned NOT NULL COMMENT '活动编号',
  `activity_code` varchar(255) NOT NULL COMMENT '期号',
  `activity_award_code` varchar(255) DEFAULT NULL COMMENT '云购中奖码',
  `activity_join_num` int(10) DEFAULT '0' COMMENT '已参与次数',
  `activity_join_needtotal_num` int(10) DEFAULT '0' COMMENT '总需人数',
  `activity_residue_need_num` int(10) DEFAULT '0' COMMENT '剩余次数',
  `activity_date` int(10) DEFAULT NULL COMMENT '活动开始时间',
  `activity_detail_sort` tinyint(1) unsigned NOT NULL DEFAULT '255' COMMENT '排序',
  PRIMARY KEY (`activity_detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='活动细节表';

-- 正在导出表  domall.do_activity_detail 的数据：~5 rows (大约)
DELETE FROM `do_activity_detail`;
/*!40000 ALTER TABLE `do_activity_detail` DISABLE KEYS */;
INSERT INTO `do_activity_detail` (`activity_detail_id`, `activity_id`, `activity_code`, `activity_award_code`, `activity_join_num`, `activity_join_needtotal_num`, `activity_residue_need_num`, `activity_date`, `activity_detail_sort`) VALUES
	(1, 1, '1', NULL, 18, 9188, 9170, 1462200293, 0),
	(2, 2, '1', NULL, 14, 102, 88, 1462323846, 0),
	(3, 3, '1', NULL, 58, 91, 33, 1462341024, 0),
	(4, 4, '1', NULL, 4, 3, -1, 1462756672, 0),
	(5, 5, '1', NULL, 16, 16, 0, 1462756734, 0);
/*!40000 ALTER TABLE `do_activity_detail` ENABLE KEYS */;

-- 导出  表 domall.do_address 结构
CREATE TABLE IF NOT EXISTS `do_address` (
  `address_id` mediumint(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '地址ID',
  `member_id` mediumint(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员ID',
  `true_name` varchar(50) NOT NULL COMMENT '会员姓名',
  `area_name` varchar(50) DEFAULT '' COMMENT '地区名称',
  `city_name` varchar(50) DEFAULT '' COMMENT '市级名称',
  `area_info` varchar(255) DEFAULT '' COMMENT '地区内容',
  `address` varchar(255) DEFAULT '' COMMENT '地址',
  `tel_phone` varchar(20) DEFAULT NULL COMMENT '座机电话',
  `mob_phone` varchar(15) DEFAULT NULL COMMENT '手机电话',
  `is_default` enum('0','1') NOT NULL DEFAULT '0' COMMENT '1默认收货地址',
  `dlyp_id` int(11) DEFAULT '0' COMMENT '自提点ID',
  PRIMARY KEY (`address_id`),
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='买家地址信息表';

-- 正在导出表  domall.do_address 的数据：~15 rows (大约)
DELETE FROM `do_address`;
/*!40000 ALTER TABLE `do_address` DISABLE KEYS */;
INSERT INTO `do_address` (`address_id`, `member_id`, `true_name`, `area_name`, `city_name`, `area_info`, `address`, `tel_phone`, `mob_phone`, `is_default`, `dlyp_id`) VALUES
	(1, 8, '陈建峰', '', '上海', '', '上海', NULL, '13799787824', '0', 0),
	(2, 8, '111', '', '111', '', '111', NULL, '111', '1', 0),
	(3, 8, '111', '', '111', '', '111', NULL, '111', '0', 0),
	(4, 8, '111', '', '111', '', '111', NULL, '111', '0', 0),
	(5, 18, '高旭龙', '', '', '', '柳市镇黄华实验学校', NULL, '1895879830', '1', 0),
	(6, 19, '陈荣', '', '', '', '国定路335号2号楼1708室', NULL, '18917115777', '1', 0),
	(7, 21, '浙江省乐清市翁洋街道华新村华新路46号', '', '', '', '乐清市翁洋街道华新村', NULL, '13587777559', '1', 0),
	(8, 16, '刘', '', '', '', '闸北', NULL, '15151970796', '0', 0),
	(9, 22, '刘', '', '', '', '闸北', NULL, '15151970796', '1', 0),
	(10, 14, '于洋', '', '', '', '小南门', NULL, '13983725804', '1', 0),
	(11, 23, '朱松', '', '', '', '桂林市雁山区雁中路18号雁山区区政府', NULL, '17777328676', '1', 0),
	(12, 16, '2', '', '', '', '2', NULL, '2', '1', 0),
	(13, 24, '18758920883', '', '', '', '雅畈镇雅新中街257号', NULL, '18758920883', '1', 0),
	(14, 25, '1', '', '', '', '1', NULL, '1', '0', 0),
	(15, 25, '2', '', '', '', '2', NULL, '2', '1', 0);
/*!40000 ALTER TABLE `do_address` ENABLE KEYS */;

-- 导出  表 domall.do_admin 结构
CREATE TABLE IF NOT EXISTS `do_admin` (
  `admin_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '管理员ID',
  `admin_name` varchar(20) NOT NULL COMMENT '管理员名称',
  `admin_avatar` varchar(100) DEFAULT NULL COMMENT '管理员头像',
  `admin_password` varchar(32) NOT NULL DEFAULT '' COMMENT '管理员密码',
  `admin_login_time` int(10) NOT NULL COMMENT '登录时间',
  `admin_login_num` int(11) NOT NULL DEFAULT '0' COMMENT '登录次数',
  `admin_is_super` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否超级管理员',
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  `admin_quick_link` varchar(400) DEFAULT NULL COMMENT '管理员常用操作',
  `member_id` int(11) DEFAULT NULL COMMENT '绑定前台帐号，可以使用前台帐号密码登录后台',
  `status` int(10) unsigned zerofill DEFAULT '0000000000' COMMENT '状态：-1禁用，0启用',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='管理员表';

-- 正在导出表  domall.do_admin 的数据：~2 rows (大约)
DELETE FROM `do_admin`;
/*!40000 ALTER TABLE `do_admin` DISABLE KEYS */;
INSERT INTO `do_admin` (`admin_id`, `admin_name`, `admin_avatar`, `admin_password`, `admin_login_time`, `admin_login_num`, `admin_is_super`, `role_id`, `admin_quick_link`, `member_id`, `status`, `create_time`, `update_time`) VALUES
	(1, 'admin', NULL, 'c4ca4238a0b923820dcc509a6f75849b', 1478520825, 41, 1, 1, NULL, 0, 0000000000, 1477732700, 1478157248),
	(2, 'test', NULL, 'c4ca4238a0b923820dcc509a6f75849b', 1477734636, 2, 0, 3, NULL, 0, 0000000000, 1477734626, NULL);
/*!40000 ALTER TABLE `do_admin` ENABLE KEYS */;

-- 导出  表 domall.do_admin_auth 结构
CREATE TABLE IF NOT EXISTS `do_admin_auth` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL DEFAULT '',
  `title` varchar(20) NOT NULL DEFAULT '',
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `url` varchar(100) NOT NULL DEFAULT '' COMMENT '链接',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `pid` smallint(5) NOT NULL COMMENT '父级ID',
  `sort` tinyint(4) NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `icon` varchar(20) NOT NULL DEFAULT '' COMMENT '图标',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=112 DEFAULT CHARSET=utf8;

-- 正在导出表  domall.do_admin_auth 的数据：12 rows
DELETE FROM `do_admin_auth`;
/*!40000 ALTER TABLE `do_admin_auth` DISABLE KEYS */;
INSERT INTO `do_admin_auth` (`id`, `name`, `title`, `type`, `url`, `status`, `pid`, `sort`, `create_time`, `icon`, `update_time`) VALUES
	(35, 'System', '系统管理', 1, '', 1, 0, 6, 1444582187, 'fa fa-cog', 1458573118),
	(37, 'Setting/index', '系统配置', 1, 'Setting/index', 1, 35, 1, 1445439984, 'fa fa-wrench', 1458313671),
	(38, 'Setting/add', '添加设置', 2, 'Setting/add', 1, 37, 1, 1445439984, '', 1458313945),
	(97, 'AdminAuth/index', '菜单管理', 1, 'AdminAuth/index', 1, 35, 2, 1458313895, '', 1458574755),
	(98, 'Index_index', '首页', 1, 'index/index', 1, 0, 0, 1458487372, 'fa fa-home', 1458577286),
	(99, 'Goods', '商品管理', 1, '', 1, 0, 2, 1458573105, 'fa fa-shopping-cart', 0),
	(100, 'Goods/index', '所有商品', 1, 'Goods/index', 1, 99, 1, 1458573826, '', 0),
	(101, 'goods_class/index', '商品分类', 1, 'goods_class/index', 1, 99, 2, 1458574179, '', 1458574698),
	(102, 'Order', '订单管理', 1, '', 1, 0, 2, 1461850807, '', 1461851082),
	(103, 'Order/index', '订单列表', 1, 'order/index', 1, 102, 1, 1461851066, '', 0),
	(104, 'Member', '客户管理', 1, '', 1, 0, 3, 1461851114, '', 0),
	(105, 'Member/index', '客户列表', 1, 'member/index', 1, 104, 1, 1461851257, '', 0),
	(110, 'Admin/index', '用户管理', 1, 'Admin/index', 1, 35, 3, 1477360303, '', 0),
	(111, 'AdminRole/index', '角色授权', 1, 'AdminRole/index', 1, 35, 4, 1477366436, '', 0);
/*!40000 ALTER TABLE `do_admin_auth` ENABLE KEYS */;

-- 导出  表 domall.do_admin_role 结构
CREATE TABLE IF NOT EXISTS `do_admin_role` (
  `role_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `role_name` varchar(100) DEFAULT NULL COMMENT '角色名称',
  `role_group` text COMMENT '权限内容',
  `role_desc` text COMMENT '描述',
  `create_time` int(11) DEFAULT NULL COMMENT '添加时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `status` tinyint(4) DEFAULT '0' COMMENT '状态：-1禁用，0是启用',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='角色权限';

-- 正在导出表  domall.do_admin_role 的数据：~0 rows (大约)
DELETE FROM `do_admin_role`;
/*!40000 ALTER TABLE `do_admin_role` DISABLE KEYS */;
INSERT INTO `do_admin_role` (`role_id`, `role_name`, `role_group`, `role_desc`, `create_time`, `update_time`, `status`) VALUES
	(1, '超级管理员', 'O:8:"stdClass":2:{s:1:"1";s:3:"100";s:1:"2";s:3:"101";}', '超级管理员权限', 1477731849, NULL, 0),
	(2, '测试1222', NULL, '12', 1477721441, NULL, 0),
	(3, '测试2', 'a:5:{i:0;s:2:"99";i:1;s:3:"100";i:2;s:2:"35";i:3;s:2:"37";i:4;s:2:"97";}', '11', 1477735776, NULL, 0);
/*!40000 ALTER TABLE `do_admin_role` ENABLE KEYS */;

-- 导出  表 domall.do_cart 结构
CREATE TABLE IF NOT EXISTS `do_cart` (
  `cart_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '购物车id',
  `buyer_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '买家id',
  `store_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '店铺id',
  `store_name` varchar(50) NOT NULL DEFAULT '' COMMENT '店铺名称',
  `goods_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商品id',
  `goods_name` varchar(100) NOT NULL COMMENT '商品名称',
  `goods_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '商品价格',
  `goods_num` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT '购买商品数量',
  `goods_image` varchar(100) NOT NULL COMMENT '商品图片',
  `bl_id` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '组合套装ID',
  PRIMARY KEY (`cart_id`),
  KEY `member_id` (`buyer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8 COMMENT='购物车数据表';

-- 正在导出表  domall.do_cart 的数据：~19 rows (大约)
DELETE FROM `do_cart`;
/*!40000 ALTER TABLE `do_cart` DISABLE KEYS */;
INSERT INTO `do_cart` (`cart_id`, `buyer_id`, `store_id`, `store_name`, `goods_id`, `goods_name`, `goods_price`, `goods_num`, `goods_image`, `bl_id`) VALUES
	(1, 2, 0, ' ', 100000, '周生生 18K金 白色黄金钻戒Belief女款 指寸12', 1.00, 1, '/uploads/2016/05/02/572767be9f2ad.jpg', 0),
	(2, 2, 0, ' ', 100000, '周生生 18K金 白色黄金钻戒Belief女款 指寸12', 1.00, 1, '/uploads/2016/05/02/572767be9f2ad.jpg', 0),
	(3, 4, 0, ' ', 100000, '周生生 18K金 白色黄金钻戒Belief女款 指寸12', 1.00, 1, '/uploads/2016/05/02/572767be9f2ad.jpg', 0),
	(4, 4, 0, ' ', 100000, '周生生 18K金 白色黄金钻戒Belief女款 指寸12', 1.00, 1, '/uploads/2016/05/02/572767be9f2ad.jpg', 0),
	(18, 13, 0, ' ', 100004, '有人提议天天', 2.00, 1, '/uploads/2016/05/09/572fe577050ac.png', 0),
	(19, 13, 0, ' ', 100002, 'e', 10.00, 1, '/uploads/2016/05/04/57298d9c9b259.jpeg', 0),
	(20, 13, 0, ' ', 100003, '分的纪录时刻规划', 23.00, 1, '/uploads/2016/05/09/572fe5334a7e4.png', 0),
	(21, 13, 0, ' ', 100001, 'd', 890.00, 2, '/uploads/2016/05/04/57294a763ba22.png', 0),
	(43, 14, 0, ' ', 100002, 'e', 10.00, 1, '/uploads/2016/05/04/57298d9c9b259.jpeg', 0),
	(47, 14, 0, ' ', 100004, '佳能 EOS 70D 套机', 2.00, 2, '/uploads/2016/05/11/573318a6c8a8a.jpg', 0),
	(50, 16, 0, ' ', 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 0),
	(55, 17, 0, ' ', 100001, 'Apple iPhone6s Plus 64G 颜色随机', 890.00, 1, '/uploads/2016/05/11/5733167027cad.jpg', 0),
	(60, 16, 0, ' ', 100004, '佳能 EOS 70D 套机', 2.00, 2, '/uploads/2016/05/11/573318a6c8a8a.jpg', 0),
	(61, 16, 0, ' ', 100003, '分的纪录时刻规划', 23.00, 3, '/uploads/2016/05/11/57331887f309d.jpg', 0),
	(79, 23, 0, ' ', 100004, '佳能 EOS 70D 套机', 2.00, 1, '/uploads/2016/05/11/573318a6c8a8a.jpg', 0),
	(80, 23, 0, ' ', 100003, '分的纪录时刻规划', 23.00, 1, '/uploads/2016/05/11/57331887f309d.jpg', 0),
	(106, 24, 0, ' ', 100001, 'Apple iPhone6s Plus 64G 颜色随机', 890.00, 2, '/uploads/2016/05/11/5733167027cad.jpg', 0),
	(107, 24, 0, ' ', 100003, '分的纪录时刻规划', 23.00, 2, '/uploads/2016/05/11/57331887f309d.jpg', 0),
	(108, 24, 0, ' ', 100004, '佳能 EOS 70D 套机', 2.00, 1, '/uploads/2016/05/11/573318a6c8a8a.jpg', 0);
/*!40000 ALTER TABLE `do_cart` ENABLE KEYS */;

-- 导出  表 domall.do_goods 结构
CREATE TABLE IF NOT EXISTS `do_goods` (
  `goods_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '商品id(SKU)',
  `goods_name` varchar(500) NOT NULL COMMENT '商品名称（+规格名称）',
  `goods_jingle` varchar(150) DEFAULT '' COMMENT '商品广告词',
  `store_id` int(10) unsigned NOT NULL COMMENT '店铺id',
  `store_name` varchar(50) NOT NULL COMMENT '店铺名称',
  `gc_id` int(10) unsigned NOT NULL COMMENT '商品分类id',
  `gc_id_1` int(10) unsigned NOT NULL COMMENT '一级分类id',
  `gc_id_2` int(10) unsigned NOT NULL COMMENT '二级分类id',
  `gc_id_3` int(10) unsigned NOT NULL COMMENT '三级分类id',
  `gc_name` varchar(200) NOT NULL COMMENT '商品分类',
  `brand_id` int(10) unsigned DEFAULT '0' COMMENT '品牌id',
  `brand_name` varchar(100) DEFAULT '' COMMENT '品牌名称',
  `goods_price` decimal(10,2) NOT NULL COMMENT '商品价格',
  `goods_promotion_price` decimal(10,2) NOT NULL COMMENT '商品促销价格',
  `goods_promotion_type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '促销类型 0无促销，1团购，2限时折扣,3一元一拍',
  `goods_promotion_num` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '活动次数，一元一拍最大期数',
  `goods_marketprice` decimal(10,2) NOT NULL COMMENT '市场价',
  `goods_costprice` decimal(10,2) NOT NULL COMMENT '成本价',
  `goods_discount` float unsigned NOT NULL COMMENT '折扣',
  `goods_serial` varchar(50) DEFAULT '' COMMENT '商品货号',
  `goods_storage_alarm` tinyint(3) unsigned NOT NULL COMMENT '库存报警值',
  `goods_barcode` varchar(20) DEFAULT '' COMMENT '商品条形码',
  `goods_click` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '商品点击数量',
  `goods_salenum` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '销售数量',
  `goods_collect` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '收藏数量',
  `spec_name` varchar(255) NOT NULL COMMENT '规格名称',
  `spec_value` text NOT NULL COMMENT '规格值',
  `goods_spec` text NOT NULL COMMENT '商品规格序列化',
  `goods_storage` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '商品库存',
  `type_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '类型id',
  `goods_image` varchar(100) NOT NULL DEFAULT '' COMMENT '商品主图',
  `goods_attr` text NOT NULL COMMENT '商品属性',
  `goods_custom` text NOT NULL COMMENT '商品自定义属性',
  `goods_body` text NOT NULL COMMENT '商品描述',
  `mobile_body` text NOT NULL COMMENT '手机端商品描述',
  `goods_state` tinyint(3) unsigned NOT NULL COMMENT '商品状态 0下架，1正常，10违规（禁售）',
  `goods_stateremark` varchar(255) DEFAULT NULL COMMENT '违规原因',
  `goods_verify` tinyint(3) unsigned NOT NULL COMMENT '商品审核 1通过，0未通过，10审核中',
  `goods_verifyremark` varchar(255) DEFAULT NULL COMMENT '审核失败原因',
  `goods_lock` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '商品锁定 0未锁，1已锁',
  `goods_addtime` int(10) unsigned NOT NULL COMMENT '商品添加时间',
  `goods_edittime` int(10) unsigned NOT NULL COMMENT '商品编辑时间',
  `goods_selltime` int(10) unsigned NOT NULL COMMENT '上架时间',
  `areaid_1` int(10) unsigned NOT NULL COMMENT '一级地区id',
  `areaid_2` int(10) unsigned NOT NULL COMMENT '二级地区id',
  `color_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '颜色规格id',
  `transport_id` mediumint(8) unsigned NOT NULL COMMENT '运费模板id',
  `transport_title` varchar(60) DEFAULT '' COMMENT '运费模板名称',
  `goods_freight` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '运费 0为免运费',
  `goods_vat` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否开具增值税发票 1是，0否',
  `goods_commend` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '商品推荐 1是，0否 默认0',
  `goods_stcids` varchar(255) DEFAULT '' COMMENT '店铺分类id 首尾用,隔开',
  `evaluation_good_star` tinyint(3) unsigned NOT NULL DEFAULT '5' COMMENT '好评星级',
  `evaluation_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '评价数',
  `is_virtual` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否为虚拟商品 1是，0否',
  `virtual_indate` int(10) unsigned NOT NULL COMMENT '虚拟商品有效期',
  `virtual_limit` tinyint(3) unsigned NOT NULL COMMENT '虚拟商品购买上限',
  `virtual_invalid_refund` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '是否允许过期退款， 1是，0否',
  `is_fcode` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否为F码商品 1是，0否',
  `is_presell` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否是预售商品 1是，0否',
  `presell_deliverdate` int(11) NOT NULL DEFAULT '0' COMMENT '预售商品发货时间',
  `is_book` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否为预定商品，1是，0否',
  `book_down_payment` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '定金金额',
  `book_final_payment` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '尾款金额',
  `book_down_time` int(11) NOT NULL DEFAULT '0' COMMENT '预定结束时间',
  `book_buyers` mediumint(9) DEFAULT '0' COMMENT '预定人数',
  `have_gift` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否拥有赠品',
  `is_own_shop` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否为平台自营',
  `contract_1` tinyint(1) NOT NULL DEFAULT '0' COMMENT '消费者保障服务状态 0关闭 1开启',
  `contract_2` tinyint(1) NOT NULL DEFAULT '0' COMMENT '消费者保障服务状态 0关闭 1开启',
  `contract_3` tinyint(1) NOT NULL DEFAULT '0' COMMENT '消费者保障服务状态 0关闭 1开启',
  `contract_4` tinyint(1) NOT NULL DEFAULT '0' COMMENT '消费者保障服务状态 0关闭 1开启',
  `contract_5` tinyint(1) NOT NULL DEFAULT '0' COMMENT '消费者保障服务状态 0关闭 1开启',
  `contract_6` tinyint(1) NOT NULL DEFAULT '0' COMMENT '消费者保障服务状态 0关闭 1开启',
  `contract_7` tinyint(1) NOT NULL DEFAULT '0' COMMENT '消费者保障服务状态 0关闭 1开启',
  `contract_8` tinyint(1) NOT NULL DEFAULT '0' COMMENT '消费者保障服务状态 0关闭 1开启',
  `contract_9` tinyint(1) NOT NULL DEFAULT '0' COMMENT '消费者保障服务状态 0关闭 1开启',
  `contract_10` tinyint(1) NOT NULL DEFAULT '0' COMMENT '消费者保障服务状态 0关闭 1开启',
  `is_chain` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否为门店商品 1是，0否',
  `plateid_top` int(10) unsigned DEFAULT NULL COMMENT '顶部关联板式',
  `plateid_bottom` int(10) unsigned DEFAULT NULL COMMENT '底部关联板式',
  `sup_id` int(11) NOT NULL COMMENT '供应商id',
  PRIMARY KEY (`goods_id`)
) ENGINE=InnoDB AUTO_INCREMENT=100005 DEFAULT CHARSET=utf8 COMMENT='商品表';

-- 正在导出表  domall.do_goods 的数据：~5 rows (大约)
DELETE FROM `do_goods`;
/*!40000 ALTER TABLE `do_goods` DISABLE KEYS */;
INSERT INTO `do_goods` (`goods_id`, `goods_name`, `goods_jingle`, `store_id`, `store_name`, `gc_id`, `gc_id_1`, `gc_id_2`, `gc_id_3`, `gc_name`, `brand_id`, `brand_name`, `goods_price`, `goods_promotion_price`, `goods_promotion_type`, `goods_promotion_num`, `goods_marketprice`, `goods_costprice`, `goods_discount`, `goods_serial`, `goods_storage_alarm`, `goods_barcode`, `goods_click`, `goods_salenum`, `goods_collect`, `spec_name`, `spec_value`, `goods_spec`, `goods_storage`, `type_id`, `goods_image`, `goods_attr`, `goods_custom`, `goods_body`, `mobile_body`, `goods_state`, `goods_stateremark`, `goods_verify`, `goods_verifyremark`, `goods_lock`, `goods_addtime`, `goods_edittime`, `goods_selltime`, `areaid_1`, `areaid_2`, `color_id`, `transport_id`, `transport_title`, `goods_freight`, `goods_vat`, `goods_commend`, `goods_stcids`, `evaluation_good_star`, `evaluation_count`, `is_virtual`, `virtual_indate`, `virtual_limit`, `virtual_invalid_refund`, `is_fcode`, `is_presell`, `presell_deliverdate`, `is_book`, `book_down_payment`, `book_final_payment`, `book_down_time`, `book_buyers`, `have_gift`, `is_own_shop`, `contract_1`, `contract_2`, `contract_3`, `contract_4`, `contract_5`, `contract_6`, `contract_7`, `contract_8`, `contract_9`, `contract_10`, `is_chain`, `plateid_top`, `plateid_bottom`, `sup_id`) VALUES
	(100000, '周生生 18K金 白色黄金钻戒Belief女款 指寸12', '', 0, ' ', 4, 0, 0, 0, '女性时尚', 0, '', 9188.00, 1.00, 3, 0, 9188.00, 9188.00, 0, '', 0, '', 0, 0, 0, '', '', '', 0, 0, '/uploads/2016/05/02/572767be9f2ad.jpg', '', '', '<p><img src="http://onegoods.nosdn.127.net/goods/980/0653c43717d53bf522e77f39784eb763.jpg"/></p>', '', 1, NULL, 1, NULL, 0, 1462323717, 1462323717, 1462323717, 0, 0, 0, 0, '', 0.00, 0, 1, '', 5, 0, 0, 1462323717, 0, 1, 0, 0, 0, 0, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0),
	(100001, 'Apple iPhone6s Plus 64G 颜色随机', '', 0, ' ', 1, 0, 0, 0, '家装配电', 0, '', 89989.00, 890.00, 3, 0, 89989.00, 89989.00, 0, '', 0, '', 0, 0, 0, '', '', '', 0, 0, '/uploads/2016/05/11/5733167027cad.jpg', '', '', '<p><br/><img src="http://onegoods.nosdn.127.net/goods/140/5e408cf99a9fd271b5ab2bc1ce6a67cd.jpg"/></p>', '', 1, NULL, 1, NULL, 0, 1462966226, 1462966226, 1462966226, 0, 0, 0, 0, '', 0.00, 0, 1, '', 5, 0, 0, 1462966226, 0, 1, 0, 0, 0, 0, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0),
	(100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', '', 0, ' ', 1, 0, 0, 0, '家装配电', 0, '', 909.00, 10.00, 3, 0, 909.00, 909.00, 0, '', 0, '', 0, 0, 0, '', '', '', 0, 0, '/uploads/2016/05/11/573318526ca1e.jpg', '', '', '<p><img src="http://onegoods.nosdn.127.net/goods/1142/165ac5458f6ccf850992de8ca2680b3d.jpg"/></p>', '', 1, NULL, 1, NULL, 0, 1462966356, 1462966356, 1462966356, 0, 0, 0, 0, '', 0.00, 0, 0, '', 5, 0, 0, 1462966356, 0, 1, 0, 0, 0, 0, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0),
	(100003, '分的纪录时刻规划', '', 0, ' ', 1, 0, 0, 0, '家装配电', 0, '', 54.00, 23.00, 3, 0, 54.00, 54.00, 0, '', 0, '', 0, 0, 0, '', '', '', 0, 0, '/uploads/2016/05/11/57331887f309d.jpg', '', '', '<p><img src="http://onegoods.nosdn.127.net/goods/145/9e06d4d8c67b253b20ad992cde76ef77.jpg"/></p>', '', 1, NULL, 1, NULL, 0, 1462966420, 1462966420, 1462966420, 0, 0, 0, 0, '', 0.00, 0, 0, '', 5, 0, 0, 1462966420, 0, 1, 0, 0, 0, 0, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0),
	(100004, '佳能 EOS 70D 套机', '', 0, ' ', 1, 0, 0, 0, '家装配电', 0, '', 32.00, 2.00, 3, 0, 32.00, 32.00, 0, '', 0, '', 0, 0, 0, '', '', '', 0, 0, '/uploads/2016/05/11/573318a6c8a8a.jpg', '', '', '<p><img src="http://onegoods.nosdn.127.net/goods/340/e0d8c650551d31240eb45ce19aeac390.jpg"/></p>', '', 1, NULL, 1, NULL, 0, 1462966453, 1462966453, 1462966453, 0, 0, 0, 0, '', 0.00, 0, 0, '', 5, 0, 0, 1462966453, 0, 1, 0, 0, 0, 0, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0);
/*!40000 ALTER TABLE `do_goods` ENABLE KEYS */;

-- 导出  表 domall.do_goods_class 结构
CREATE TABLE IF NOT EXISTS `do_goods_class` (
  `gc_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '索引ID',
  `gc_name` varchar(100) NOT NULL COMMENT '分类名称',
  `type_id` int(10) unsigned DEFAULT '0' COMMENT '类型id',
  `type_name` varchar(100) DEFAULT '' COMMENT '类型名称',
  `gc_parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父ID',
  `commis_rate` float unsigned NOT NULL DEFAULT '0' COMMENT '佣金比例',
  `gc_sort` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `gc_virtual` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否允许发布虚拟商品，1是，0否',
  `gc_title` varchar(200) DEFAULT '' COMMENT '名称',
  `gc_keywords` varchar(255) DEFAULT '' COMMENT '关键词',
  `gc_description` varchar(255) DEFAULT '' COMMENT '描述',
  `show_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '商品展示方式，1按颜色，2按SPU',
  `is_show` tinyint(3) DEFAULT '1' COMMENT '是否显示在首页：1显示，0不显示',
  `gc_image` varchar(100) DEFAULT '' COMMENT '分类图标',
  PRIMARY KEY (`gc_id`),
  KEY `store_id` (`gc_parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='商品分类表';

-- 正在导出表  domall.do_goods_class 的数据：~13 rows (大约)
DELETE FROM `do_goods_class`;
/*!40000 ALTER TABLE `do_goods_class` DISABLE KEYS */;
INSERT INTO `do_goods_class` (`gc_id`, `gc_name`, `type_id`, `type_name`, `gc_parent_id`, `commis_rate`, `gc_sort`, `gc_virtual`, `gc_title`, `gc_keywords`, `gc_description`, `show_type`, `is_show`, `gc_image`) VALUES
	(1, '家装配电', 0, '', 0, 0, 1, 0, '', '', '', 1, 1, '/uploads/2016/05/11/573315f23c433.jpg'),
	(2, '工艺木雕', 0, '', 0, 0, 0, 0, '', '', '', 1, 1, '/uploads/2016/05/11/573315f82e602.jpg'),
	(3, '女神微整', 0, '', 0, 0, 0, 0, '', '', '', 1, 1, '/uploads/2016/05/11/573315fd8d3a0.jpg'),
	(4, '教育培训', 0, '', 0, 0, 0, 0, '', '', '', 1, 1, '/uploads/2016/05/11/573316064b380.jpg'),
	(5, '旅游度假', 0, '', 0, 0, 0, 0, '', '', '', 1, 1, '/uploads/2016/05/11/5733160cda5d0.jpg'),
	(6, '交友相亲', 0, '', 0, 0, 0, 0, '', '', '', 1, 1, '/uploads/2016/05/11/5733161323766.jpg'),
	(7, '大健康系', 0, '', 0, 0, 0, 0, '', '', '', 1, 1, '/uploads/2016/05/11/57331619831e6.jpg'),
	(8, '苹果专区', 0, '', 0, 0, 0, 0, '', '', '', 1, 1, '/uploads/2016/05/11/5733161fb4b9c.jpg'),
	(9, '手机平板', 0, '', 0, 0, 0, 0, '', '', '', 1, 1, '/uploads/2016/05/11/57331625d066b.jpg'),
	(10, '电脑办公', 0, '', 0, 0, 0, 0, '', '', '', 1, 1, '/uploads/2016/05/11/5733162bf1ba1.jpg'),
	(11, '数码影音', 0, '', 0, 0, 0, 0, '', '', '', 1, 1, '/uploads/2016/05/11/57331631c8999.jpg'),
	(12, '美食天地', 0, '', 0, 0, 0, 0, '', '', '', 1, 1, '/uploads/2016/05/11/57331638084e0.jpg'),
	(13, '潮流新品', 0, '', 0, 0, 0, 0, '', '', '', 1, 1, '/uploads/2016/05/11/5733163e02f39.jpg');
/*!40000 ALTER TABLE `do_goods_class` ENABLE KEYS */;

-- 导出  表 domall.do_member 结构
CREATE TABLE IF NOT EXISTS `do_member` (
  `member_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '会员id',
  `member_name` varchar(50) NOT NULL COMMENT '会员名称',
  `member_truename` varchar(20) DEFAULT NULL COMMENT '真实姓名',
  `member_avatar` varchar(50) DEFAULT NULL COMMENT '会员头像',
  `member_sex` tinyint(1) DEFAULT NULL COMMENT '会员性别',
  `member_birthday` date DEFAULT NULL COMMENT '生日',
  `member_passwd` varchar(32) NOT NULL COMMENT '会员密码',
  `member_paypwd` char(32) DEFAULT NULL COMMENT '支付密码',
  `member_email` varchar(100) NOT NULL COMMENT '会员邮箱',
  `member_email_bind` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0未绑定1已绑定',
  `member_mobile` varchar(11) DEFAULT NULL COMMENT '手机号',
  `member_mobile_bind` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0未绑定1已绑定',
  `member_qq` varchar(100) DEFAULT NULL COMMENT 'qq',
  `member_ww` varchar(100) DEFAULT NULL COMMENT '阿里旺旺',
  `member_login_num` int(11) NOT NULL DEFAULT '1' COMMENT '登录次数',
  `member_time` varchar(10) NOT NULL COMMENT '会员注册时间',
  `member_login_time` varchar(10) NOT NULL COMMENT '当前登录时间',
  `member_old_login_time` varchar(10) NOT NULL COMMENT '上次登录时间',
  `member_login_ip` varchar(20) DEFAULT NULL COMMENT '当前登录ip',
  `member_old_login_ip` varchar(20) DEFAULT NULL COMMENT '上次登录ip',
  `member_qqopenid` varchar(100) DEFAULT NULL COMMENT 'qq互联id',
  `member_qqinfo` text COMMENT 'qq账号相关信息',
  `member_sinaopenid` varchar(100) DEFAULT NULL COMMENT '新浪微博登录id',
  `member_sinainfo` text COMMENT '新浪账号相关信息序列化值',
  `weixin_unionid` varchar(50) DEFAULT NULL COMMENT '微信用户统一标识',
  `weixin_info` varchar(255) DEFAULT NULL COMMENT '微信用户相关信息',
  `member_points` int(11) NOT NULL DEFAULT '0' COMMENT '会员积分',
  `available_predeposit` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '预存款可用金额',
  `freeze_predeposit` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '预存款冻结金额',
  `available_rc_balance` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '可用充值卡余额',
  `freeze_rc_balance` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '冻结充值卡余额',
  `inform_allow` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否允许举报(1可以/2不可以)',
  `is_buy` tinyint(1) NOT NULL DEFAULT '1' COMMENT '会员是否有购买权限 1为开启 0为关闭',
  `is_allowtalk` tinyint(1) NOT NULL DEFAULT '1' COMMENT '会员是否有咨询和发送站内信的权限 1为开启 0为关闭',
  `member_state` tinyint(1) NOT NULL DEFAULT '1' COMMENT '会员的开启状态 1为开启 0为关闭',
  `member_snsvisitnum` int(11) NOT NULL DEFAULT '0' COMMENT 'sns空间访问次数',
  `member_areaid` int(11) DEFAULT NULL COMMENT '地区ID',
  `member_cityid` int(11) DEFAULT NULL COMMENT '城市ID',
  `member_provinceid` int(11) DEFAULT NULL COMMENT '省份ID',
  `member_areainfo` varchar(255) DEFAULT NULL COMMENT '地区内容',
  `member_privacy` text COMMENT '隐私设定',
  `member_exppoints` int(11) NOT NULL DEFAULT '0' COMMENT '会员经验值',
  `invite_one` int(10) DEFAULT '0' COMMENT '一级会员',
  `invite_two` int(10) DEFAULT '0' COMMENT '二级会员',
  `invite_three` int(10) DEFAULT '0' COMMENT '三级会员',
  PRIMARY KEY (`member_id`),
  KEY `member_name` (`member_name`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='会员表';

-- 正在导出表  domall.do_member 的数据：~25 rows (大约)
DELETE FROM `do_member`;
/*!40000 ALTER TABLE `do_member` DISABLE KEYS */;
INSERT INTO `do_member` (`member_id`, `member_name`, `member_truename`, `member_avatar`, `member_sex`, `member_birthday`, `member_passwd`, `member_paypwd`, `member_email`, `member_email_bind`, `member_mobile`, `member_mobile_bind`, `member_qq`, `member_ww`, `member_login_num`, `member_time`, `member_login_time`, `member_old_login_time`, `member_login_ip`, `member_old_login_ip`, `member_qqopenid`, `member_qqinfo`, `member_sinaopenid`, `member_sinainfo`, `weixin_unionid`, `weixin_info`, `member_points`, `available_predeposit`, `freeze_predeposit`, `available_rc_balance`, `freeze_rc_balance`, `inform_allow`, `is_buy`, `is_allowtalk`, `member_state`, `member_snsvisitnum`, `member_areaid`, `member_cityid`, `member_provinceid`, `member_areainfo`, `member_privacy`, `member_exppoints`, `invite_one`, `invite_two`, `invite_three`) VALUES
	(1, 'sdad', NULL, NULL, NULL, NULL, 'bd523196c81ea7b7ad1ed1ea100d597a', NULL, 'sdad', 0, NULL, 0, NULL, NULL, 1, '1462241299', '1462241299', '1462241299', '101.81.30.73', '101.81.30.73', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0.00, 0.00, 0.00, 0.00, 1, 1, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
	(2, 'dsaf', NULL, NULL, NULL, NULL, '5c7d0c90cf9e0ce560956179e8e82e7d', NULL, 'dsaf', 0, NULL, 0, NULL, NULL, 1, '1462243327', '1462243327', '1462243327', '101.81.30.73', '101.81.30.73', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0.00, 0.00, 0.00, 0.00, 1, 1, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
	(3, 'dsaf', NULL, NULL, NULL, NULL, '5c7d0c90cf9e0ce560956179e8e82e7d', NULL, 'dsaf', 0, NULL, 0, NULL, NULL, 1, '1462243327', '1462243327', '1462243327', '101.81.30.73', '101.81.30.73', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0.00, 0.00, 0.00, 0.00, 1, 1, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
	(4, 'dsa', NULL, NULL, NULL, NULL, '1ff41c04801d2b57ba04d8ad88978cd8', NULL, 'dsa', 0, NULL, 0, NULL, NULL, 1, '1462243791', '1462243791', '1462243791', '101.81.30.73', '101.81.30.73', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0.00, 0.00, 0.00, 0.00, 1, 1, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
	(5, 'dsa', NULL, NULL, NULL, NULL, '1ff41c04801d2b57ba04d8ad88978cd8', NULL, 'dsa', 0, NULL, 0, NULL, NULL, 1, '1462243791', '1462243791', '1462243791', '101.81.30.73', '101.81.30.73', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0.00, 0.00, 0.00, 0.00, 1, 1, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
	(6, 'dd', NULL, NULL, NULL, NULL, '1aabac6d068eef6a7bad3fdf50a05cc8', NULL, 'dd', 0, NULL, 0, NULL, NULL, 1, '1462244667', '1462244667', '1462244667', '101.81.30.73', '101.81.30.73', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0.00, 0.00, 0.00, 0.00, 1, 1, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
	(7, 'dd', NULL, NULL, NULL, NULL, '1aabac6d068eef6a7bad3fdf50a05cc8', NULL, 'dd', 0, NULL, 0, NULL, NULL, 1, '1462244667', '1462244667', '1462244667', '101.81.30.73', '101.81.30.73', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0.00, 0.00, 0.00, 0.00, 1, 1, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
	(8, 'idowe', NULL, NULL, NULL, NULL, 'c4ca4238a0b923820dcc509a6f75849b', NULL, 'idowe', 0, NULL, 0, NULL, NULL, 1, '1462245111', '1462245111', '1462245111', '101.81.30.73', '101.81.30.73', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0.00, 0.00, 0.00, 0.00, 1, 1, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
	(9, 'dsaa', NULL, NULL, NULL, NULL, '8d41627e46d5b8556d0d3e30ec15538e', NULL, 'dsaa', 0, NULL, 0, NULL, NULL, 1, '1462330545', '1462330545', '1462330545', '101.81.30.73', '101.81.30.73', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0.00, 0.00, 0.00, 0.00, 1, 1, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
	(10, 'dsaa', NULL, NULL, NULL, NULL, '8d41627e46d5b8556d0d3e30ec15538e', NULL, 'dsaa', 0, NULL, 0, NULL, NULL, 1, '1462330545', '1462330545', '1462330545', '101.81.30.73', '101.81.30.73', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0.00, 0.00, 0.00, 0.00, 1, 1, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
	(11, 'dsadsda', NULL, NULL, NULL, NULL, '8d41627e46d5b8556d0d3e30ec15538e', NULL, 'dsadsda', 0, NULL, 0, NULL, NULL, 1, '1462330553', '1462330553', '1462330553', '101.81.30.73', '101.81.30.73', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0.00, 0.00, 0.00, 0.00, 1, 1, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
	(12, 'dsadsda', NULL, NULL, NULL, NULL, '8d41627e46d5b8556d0d3e30ec15538e', NULL, 'dsadsda', 0, NULL, 0, NULL, NULL, 1, '1462330553', '1462330553', '1462330553', '101.81.30.73', '101.81.30.73', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0.00, 0.00, 0.00, 0.00, 1, 1, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
	(13, '15151970799', NULL, NULL, NULL, NULL, 'ac627ab1ccbdb62ec96e702f07f6425b', NULL, '15151970799', 0, NULL, 0, NULL, NULL, 1, '1462770443', '1462770443', '1462770443', '101.81.30.73', '101.81.30.73', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0.00, 0.00, 0.00, 0.00, 1, 1, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
	(14, '13983725804', NULL, NULL, NULL, NULL, '3e4a83ba105b18865b03b46e11f42fa4', NULL, '13983725804', 0, NULL, 0, NULL, NULL, 1, '1462867168', '1462867168', '1462867168', '101.81.30.73', '101.81.30.73', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0.00, 0.00, 0.00, 0.00, 1, 1, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
	(15, '13799787824', NULL, NULL, NULL, NULL, 'c4ca4238a0b923820dcc509a6f75849b', NULL, '13799787824', 0, NULL, 0, NULL, NULL, 1, '1462955710', '1462955710', '1462955710', '101.81.30.73', '101.81.30.73', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0.00, 0.00, 0.00, 0.00, 1, 1, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
	(16, '15151970796', NULL, NULL, NULL, NULL, '26657d5ff9020d2abefe558796b99584', NULL, '15151970796', 0, NULL, 0, NULL, NULL, 1, '1463034051', '1463034051', '1463034051', '101.81.30.73', '101.81.30.73', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0.00, 0.00, 0.00, 0.00, 1, 1, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
	(17, '13915833338', NULL, NULL, NULL, NULL, 'b09e22391ad160f55c7de1f4cb5e2646', NULL, '13915833338', 0, NULL, 0, NULL, NULL, 1, '1463142739', '1463142739', '1463142739', '180.115.39.223', '180.115.39.223', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0.00, 0.00, 0.00, 0.00, 1, 1, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
	(18, '18958798530', NULL, NULL, NULL, NULL, 'fb827a6bb165c4fc3818063ec2b37003', NULL, '18958798530', 0, NULL, 0, NULL, NULL, 1, '1463147716', '1463147716', '1463147716', '36.23.17.197', '36.23.17.197', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0.00, 0.00, 0.00, 0.00, 1, 1, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
	(19, '18917115777', NULL, NULL, NULL, NULL, 'c42d6d8b416847a57bb9011bc26a9673', NULL, '18917115777', 0, NULL, 0, NULL, NULL, 1, '1463149867', '1463149867', '1463149867', '222.70.169.230', '222.70.169.230', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0.00, 0.00, 0.00, 0.00, 1, 1, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
	(20, '13968729296', NULL, NULL, NULL, NULL, 'dedccf77b1a5c42a22236a1157c62679', NULL, '13968729296', 0, NULL, 0, NULL, NULL, 1, '1463150393', '1463150393', '1463150393', '60.180.178.177', '60.180.178.177', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0.00, 0.00, 0.00, 0.00, 1, 1, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
	(21, '13587777559', NULL, NULL, NULL, NULL, 'b8acafaddda44785abfae5f9750b3546', NULL, '13587777559', 0, NULL, 0, NULL, NULL, 1, '1463153490', '1463153490', '1463153490', '112.17.244.44', '112.17.244.44', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0.00, 0.00, 0.00, 0.00, 1, 1, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
	(22, '15151970711', NULL, NULL, NULL, NULL, '6512bd43d9caa6e02c990b0a82652dca', NULL, '15151970711', 0, NULL, 0, NULL, NULL, 1, '1463391885', '1463391885', '1463391885', '101.81.30.73', '101.81.30.73', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0.00, 0.00, 0.00, 0.00, 1, 1, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
	(23, '17777328676', NULL, NULL, NULL, NULL, '22c41bc4750a2c10fb8ace42c86522ce', NULL, '17777328676', 0, NULL, 0, NULL, NULL, 1, '1463491234', '1463491234', '1463491234', '171.105.144.48', '171.105.144.48', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0.00, 0.00, 0.00, 0.00, 1, 1, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
	(24, '18758920883', NULL, NULL, NULL, NULL, '31349afd2830229e61e2c7ff73565932', NULL, '18758920883', 0, NULL, 0, NULL, NULL, 1, '1463642896', '1463642896', '1463642896', '59.34.41.5', '59.34.41.5', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0.00, 0.00, 0.00, 0.00, 1, 1, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
	(25, '15151970712', NULL, NULL, NULL, NULL, 'c20ad4d76fe97759aa27a0c99bff6710', NULL, '15151970712', 0, NULL, 0, NULL, NULL, 1, '1463706829', '1463706829', '1463706829', '180.168.140.70', '180.168.140.70', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0.00, 0.00, 0.00, 0.00, 1, 1, 1, 1, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0);
/*!40000 ALTER TABLE `do_member` ENABLE KEYS */;

-- 导出  表 domall.do_orders 结构
CREATE TABLE IF NOT EXISTS `do_orders` (
  `order_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '订单索引id',
  `order_sn` varchar(20) NOT NULL COMMENT '订单编号',
  `pay_sn` varchar(20) NOT NULL COMMENT '支付单号',
  `store_id` int(11) unsigned NOT NULL COMMENT '卖家店铺id',
  `store_name` varchar(50) NOT NULL COMMENT '卖家店铺名称',
  `buyer_id` int(11) unsigned NOT NULL COMMENT '买家id',
  `buyer_name` varchar(50) NOT NULL COMMENT '买家姓名',
  `buyer_email` varchar(80) DEFAULT NULL COMMENT '买家电子邮箱',
  `buyer_phone` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '买家手机',
  `add_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '订单生成时间',
  `payment_code` char(10) NOT NULL DEFAULT '' COMMENT '支付方式名称代码',
  `payment_time` int(10) unsigned DEFAULT '0' COMMENT '支付(付款)时间',
  `finnshed_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '订单完成时间',
  `goods_amount` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '商品总价格',
  `order_amount` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '订单总价格',
  `rcb_amount` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '充值卡支付金额',
  `pd_amount` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '预存款支付金额',
  `shipping_fee` decimal(10,2) unsigned DEFAULT '0.00' COMMENT '运费',
  `evaluation_state` tinyint(4) DEFAULT '0' COMMENT '评价状态 0未评价，1已评价，2已过期未评价',
  `evaluation_again_state` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '追加评价状态 0未评价，1已评价，2已过期未评价',
  `order_state` tinyint(4) NOT NULL DEFAULT '10' COMMENT '订单状态：0(已取消)10(默认):未付款;20:已付款;30:已发货;40:已收货;',
  `refund_state` tinyint(4) unsigned DEFAULT '0' COMMENT '退款状态:0是无退款,1是部分退款,2是全部退款',
  `lock_state` tinyint(4) unsigned DEFAULT '0' COMMENT '锁定状态:0是正常,大于0是锁定,默认是0',
  `delete_state` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除状态0未删除1放入回收站2彻底删除',
  `refund_amount` decimal(10,2) DEFAULT '0.00' COMMENT '退款金额',
  `delay_time` int(10) unsigned DEFAULT '0' COMMENT '延迟时间,默认为0',
  `order_from` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1WEB2mobile',
  `shipping_code` varchar(50) DEFAULT '' COMMENT '物流单号',
  `order_type` tinyint(4) DEFAULT '1' COMMENT '订单类型1普通订单(默认),2预定订单,3门店自提订单',
  `api_pay_time` int(10) unsigned DEFAULT '0' COMMENT '在线支付动作时间,只要向第三方支付平台提交就会更新',
  `api_pay_state` enum('0','1') DEFAULT '0' COMMENT '0默认未支付1已支付(只有第三方支付接口通知到时才会更改此状态)',
  `chain_id` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '自提门店ID',
  `chain_code` mediumint(6) unsigned NOT NULL DEFAULT '0' COMMENT '门店提货码',
  `rpt_amount` decimal(10,2) unsigned DEFAULT '0.00' COMMENT '红包值',
  `trade_no` varchar(50) DEFAULT NULL COMMENT '外部交易订单号',
  `shipping_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '配送时间',
  `shipping_express_id` tinyint(1) NOT NULL DEFAULT '0' COMMENT '配送公司ID',
  `evaluation_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '评价时间',
  `evalseller_time` int(10) unsigned DEFAULT NULL COMMENT '卖家评价买家的时间',
  `order_message` varchar(300) DEFAULT NULL COMMENT '订单留言',
  `order_pointscount` int(11) NOT NULL DEFAULT '0' COMMENT '订单赠送积分',
  `voucher_price` int(11) DEFAULT NULL COMMENT '代金券面额',
  `voucher_code` varchar(32) DEFAULT NULL COMMENT '代金券编码',
  `deliver_explain` text COMMENT '发货备注',
  `daddress_id` mediumint(9) NOT NULL DEFAULT '0' COMMENT '发货地址ID',
  `reciver_address_id` mediumint(10) NOT NULL COMMENT '收货地址ID',
  `reciver_name` varchar(50) DEFAULT '' COMMENT '收货人姓名',
  `reciver_info` varchar(500) DEFAULT '' COMMENT '收货人其它信息',
  `reciver_city_name` varchar(50) DEFAULT '' COMMENT '收货人市级名称',
  `reciver_area_name` varchar(50) DEFAULT '' COMMENT '收货人地区名称',
  `reciver_area_info` varchar(50) DEFAULT '' COMMENT '收货人地区内容',
  `reciver_address` varchar(255) DEFAULT '' COMMENT '收货人地址',
  `reciver_tel_phone` varchar(20) DEFAULT NULL COMMENT '收货人座机电话',
  `reciver_mob_phone` varchar(15) DEFAULT NULL COMMENT '收货人手机电话',
  `invoice_info` varchar(500) DEFAULT '' COMMENT '发票信息',
  `promotion_info` varchar(800) DEFAULT '' COMMENT '促销信息备注',
  `dlyo_pickup_code` varchar(6) DEFAULT NULL COMMENT '提货码',
  `promotion_total` decimal(10,2) DEFAULT '0.00' COMMENT '订单总优惠金额（代金券，满减，平台红包）',
  `discount` tinyint(4) DEFAULT '0' COMMENT '会员折扣x%',
  `charge_id` varchar(100) DEFAULT NULL COMMENT 'ping++支付返回ID',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8 COMMENT='订单表';

-- 正在导出表  domall.do_orders 的数据：~3 rows (大约)
DELETE FROM `do_orders`;
/*!40000 ALTER TABLE `do_orders` DISABLE KEYS */;
INSERT INTO `do_orders` (`order_id`, `order_sn`, `pay_sn`, `store_id`, `store_name`, `buyer_id`, `buyer_name`, `buyer_email`, `buyer_phone`, `add_time`, `payment_code`, `payment_time`, `finnshed_time`, `goods_amount`, `order_amount`, `rcb_amount`, `pd_amount`, `shipping_fee`, `evaluation_state`, `evaluation_again_state`, `order_state`, `refund_state`, `lock_state`, `delete_state`, `refund_amount`, `delay_time`, `order_from`, `shipping_code`, `order_type`, `api_pay_time`, `api_pay_state`, `chain_id`, `chain_code`, `rpt_amount`, `trade_no`, `shipping_time`, `shipping_express_id`, `evaluation_time`, `evalseller_time`, `order_message`, `order_pointscount`, `voucher_price`, `voucher_code`, `deliver_explain`, `daddress_id`, `reciver_address_id`, `reciver_name`, `reciver_info`, `reciver_city_name`, `reciver_area_name`, `reciver_area_info`, `reciver_address`, `reciver_tel_phone`, `reciver_mob_phone`, `invoice_info`, `promotion_info`, `dlyo_pickup_code`, `promotion_total`, `discount`, `charge_id`) VALUES
	(118, '8000000000096308', '500517181181963008', 0, ' ', 8, 'idowe', 'idowe', 111, 1463837181, 'online', 0, 0, 890.00, 0.01, 0.00, 0.00, 0.00, 0, 0, 10, 0, 0, 0, 0.00, 0, 1, '', 1, 0, '0', 0, 0, 0.00, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, 0, 2, '111', '', '111', '', '', '111', NULL, '111', '', '', NULL, 0.00, 0, 'ch_ajXvvPWHSyTCmTK4CGPyrDyT'),
	(119, '8000000000085308', '290517181266852008', 0, ' ', 8, 'idowe', 'idowe', 111, 1463837266, 'alipay_wap', 0, 0, 890.00, 0.01, 0.00, 0.00, 0.00, 0, 0, 10, 0, 0, 0, 0.00, 0, 1, '', 1, 1463837309, '1', 0, 0, 0.00, '2016052121001004650220331030', 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, 0, 2, '111', '', '111', '', '', '111', NULL, '111', '', '', NULL, 0.00, 0, 'ch_ybjrDO5yDuDGm9iTi5ujDCGO'),
	(120, '8000000000040808', '480517181337408008', 0, ' ', 8, 'idowe', 'idowe', 111, 1463837337, 'online', 0, 0, 890.00, 0.01, 0.00, 0.00, 0.00, 0, 0, 10, 0, 0, 0, 0.00, 0, 1, '', 1, 0, '0', 0, 0, 0.00, NULL, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, 0, 2, '111', '', '111', '', '', '111', NULL, '111', '', '', NULL, 0.00, 0, 'ch_bnjPOKuvPa90S8ybzTOy1ej1');
/*!40000 ALTER TABLE `do_orders` ENABLE KEYS */;

-- 导出  表 domall.do_order_goods 结构
CREATE TABLE IF NOT EXISTS `do_order_goods` (
  `rec_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '订单商品表索引id',
  `order_id` int(11) NOT NULL COMMENT '订单id',
  `goods_id` int(11) NOT NULL COMMENT '商品id',
  `goods_name` varchar(500) NOT NULL COMMENT '商品名称',
  `goods_price` decimal(10,2) NOT NULL COMMENT '商品价格',
  `goods_num` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT '商品数量',
  `goods_image` varchar(100) DEFAULT NULL COMMENT '商品图片',
  `goods_pay_price` decimal(10,2) unsigned NOT NULL COMMENT '商品实际成交价',
  `store_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '店铺ID',
  `buyer_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '买家ID',
  `goods_type` char(1) NOT NULL DEFAULT '1' COMMENT '1默认2团购商品3一元云购',
  `promotions_id` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '促销活动ID（团购ID/限时折扣ID/优惠套装ID）与goods_type搭配使用',
  `commis_rate` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '佣金比例',
  `gc_id` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '商品最底级分类ID',
  `goods_spec` varchar(255) DEFAULT NULL COMMENT '商品规格',
  `goods_contractid` varchar(100) DEFAULT NULL COMMENT '商品开启的消费者保障服务id',
  `activity_detail_id` int(10) DEFAULT NULL COMMENT '活动明细表ID',
  `goods_bid_state` tinyint(1) DEFAULT '0' COMMENT '是否中奖（0已揭晓/1中奖/2未中）',
  PRIMARY KEY (`rec_id`)
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8 COMMENT='订单商品表';

-- 正在导出表  domall.do_order_goods 的数据：~101 rows (大约)
DELETE FROM `do_order_goods`;
/*!40000 ALTER TABLE `do_order_goods` DISABLE KEYS */;
INSERT INTO `do_order_goods` (`rec_id`, `order_id`, `goods_id`, `goods_name`, `goods_price`, `goods_num`, `goods_image`, `goods_pay_price`, `store_id`, `buyer_id`, `goods_type`, `promotions_id`, `commis_rate`, `gc_id`, `goods_spec`, `goods_contractid`, `activity_detail_id`, `goods_bid_state`) VALUES
	(1, 1, 100000, '周生生 18K金 白色黄金钻戒Belief女款 指寸12', 1.00, 4, '/uploads/2016/05/02/572767be9f2ad.jpg', 4.00, 0, 8, '3', 3, 200, 4, NULL, NULL, 1, 0),
	(2, 2, 100000, '周生生 18K金 白色黄金钻戒Belief女款 指寸12', 1.00, 2, '/uploads/2016/05/02/572767be9f2ad.jpg', 2.00, 0, 8, '3', 3, 200, 4, NULL, NULL, 1, 0),
	(3, 3, 100000, '周生生 18K金 白色黄金钻戒Belief女款 指寸12', 1.00, 1, '/uploads/2016/05/02/572767be9f2ad.jpg', 1.00, 0, 8, '3', 3, 200, 4, NULL, NULL, 1, 0),
	(4, 4, 100002, 'e', 432.00, 1, '/uploads/2016/05/04/57298d9c9b259.jpeg', 432.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(5, 5, 100000, '周生生 18K金 白色黄金钻戒Belief女款 指寸12', 1.00, 1, '/uploads/2016/05/02/572767be9f2ad.jpg', 1.00, 0, 8, '3', 3, 200, 4, NULL, NULL, 1, 0),
	(6, 6, 100000, '周生生 18K金 白色黄金钻戒Belief女款 指寸12', 1.00, 1, '/uploads/2016/05/02/572767be9f2ad.jpg', 1.00, 0, 8, '3', 3, 200, 4, NULL, NULL, 1, 0),
	(7, 7, 100000, '周生生 18K金 白色黄金钻戒Belief女款 指寸12', 1.00, 1, '/uploads/2016/05/02/572767be9f2ad.jpg', 1.00, 0, 8, '3', 3, 200, 4, NULL, NULL, 1, 0),
	(8, 8, 100000, '周生生 18K金 白色黄金钻戒Belief女款 指寸12', 1.00, 1, '/uploads/2016/05/02/572767be9f2ad.jpg', 1.00, 0, 8, '3', 3, 200, 4, NULL, NULL, 1, 0),
	(9, 9, 100001, 'd', 890.00, 2, '/uploads/2016/05/04/57294a763ba22.png', 1780.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 2, 0),
	(10, 10, 100000, '周生生 18K金 白色黄金钻戒Belief女款 指寸12', 1.00, 1, '/uploads/2016/05/02/572767be9f2ad.jpg', 1.00, 0, 8, '3', 3, 200, 4, NULL, NULL, 1, 0),
	(11, 11, 100002, 'e', 432.00, 2, '/uploads/2016/05/04/57298d9c9b259.jpeg', 864.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(12, 12, 100001, 'd', 890.00, 1, '/uploads/2016/05/04/57294a763ba22.png', 890.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 2, 0),
	(24, 1, 1, '1', 1.00, 11, '1', 1.00, 1, 1, '1', 0, 0, 0, NULL, NULL, NULL, 0),
	(27, 19, 100002, 'e', 432.00, 7, '/uploads/2016/05/04/57298d9c9b259.jpeg', 3024.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(28, 19, 100001, 'd', 890.00, 1, '/uploads/2016/05/04/57294a763ba22.png', 890.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 2, 0),
	(29, 20, 100002, 'e', 10.00, 1, '/uploads/2016/05/04/57298d9c9b259.jpeg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(30, 21, 100002, 'e', 10.00, 1, '/uploads/2016/05/04/57298d9c9b259.jpeg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(31, 22, 100002, 'e', 10.00, 1, '/uploads/2016/05/04/57298d9c9b259.jpeg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(32, 23, 100002, 'e', 10.00, 1, '/uploads/2016/05/04/57298d9c9b259.jpeg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(33, 24, 100002, 'e', 10.00, 1, '/uploads/2016/05/04/57298d9c9b259.jpeg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(34, 25, 100000, '周生生 18K金 白色黄金钻戒Belief女款 指寸12', 1.00, 1, '/uploads/2016/05/02/572767be9f2ad.jpg', 1.00, 0, 8, '3', 3, 200, 4, NULL, NULL, 1, 0),
	(37, 28, 100001, 'd', 890.00, 1, '/uploads/2016/05/04/57294a763ba22.png', 890.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 2, 0),
	(38, 29, 100001, 'd', 890.00, 1, '/uploads/2016/05/04/57294a763ba22.png', 890.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 2, 0),
	(39, 30, 100004, '有人提议天天', 2.00, 1, '/uploads/2016/05/09/572fe577050ac.png', 2.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 5, 0),
	(40, 31, 100003, '分的纪录时刻规划', 23.00, 1, '/uploads/2016/05/09/572fe5334a7e4.png', 23.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 4, 0),
	(41, 32, 100002, 'e', 10.00, 1, '/uploads/2016/05/04/57298d9c9b259.jpeg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(42, 33, 100001, 'd', 890.00, 1, '/uploads/2016/05/04/57294a763ba22.png', 890.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 2, 0),
	(43, 34, 100003, '分的纪录时刻规划', 23.00, 1, '/uploads/2016/05/09/572fe5334a7e4.png', 23.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 4, 0),
	(44, 35, 100001, 'd', 890.00, 1, '/uploads/2016/05/04/57294a763ba22.png', 890.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 2, 0),
	(45, 36, 100004, '有人提议天天', 2.00, 1, '/uploads/2016/05/09/572fe577050ac.png', 2.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 5, 0),
	(46, 37, 100004, '有人提议天天', 2.00, 1, '/uploads/2016/05/09/572fe577050ac.png', 2.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 5, 0),
	(47, 38, 100001, 'd', 890.00, 1, '/uploads/2016/05/04/57294a763ba22.png', 890.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 2, 0),
	(48, 39, 100004, '有人提议天天', 2.00, 1, '/uploads/2016/05/09/572fe577050ac.png', 2.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 5, 0),
	(49, 40, 100001, 'd', 890.00, 1, '/uploads/2016/05/04/57294a763ba22.png', 890.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 2, 0),
	(50, 41, 100004, '有人提议天天', 2.00, 1, '/uploads/2016/05/09/572fe577050ac.png', 2.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 5, 0),
	(51, 42, 100004, '有人提议天天', 2.00, 1, '/uploads/2016/05/09/572fe577050ac.png', 2.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 5, 0),
	(52, 43, 100001, 'd', 890.00, 1, '/uploads/2016/05/04/57294a763ba22.png', 890.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 2, 0),
	(53, 44, 100000, '周生生 18K金 白色黄金钻戒Belief女款 指寸12', 1.00, 2, '/uploads/2016/05/02/572767be9f2ad.jpg', 2.00, 0, 8, '3', 3, 200, 4, NULL, NULL, 1, 0),
	(54, 45, 100004, '有人提议天天', 2.00, 1, '/uploads/2016/05/09/572fe577050ac.png', 2.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 5, 0),
	(55, 46, 100000, '周生生 18K金 白色黄金钻戒Belief女款 指寸12', 1.00, 1, '/uploads/2016/05/02/572767be9f2ad.jpg', 1.00, 0, 8, '3', 3, 200, 4, NULL, NULL, 1, 0),
	(56, 47, 100001, 'd', 890.00, 1, '/uploads/2016/05/04/57294a763ba22.png', 890.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 2, 0),
	(57, 48, 100000, '周生生 18K金 白色黄金钻戒Belief女款 指寸12', 1.00, 1, '/uploads/2016/05/02/572767be9f2ad.jpg', 1.00, 0, 8, '3', 3, 200, 4, NULL, NULL, 1, 0),
	(58, 49, 100004, '有人提议天天', 2.00, 1, '/uploads/2016/05/09/572fe577050ac.png', 2.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 5, 0),
	(59, 50, 100003, '分的纪录时刻规划', 23.00, 2, '/uploads/2016/05/11/57331887f309d.jpg', 46.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 4, 0),
	(60, 50, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(61, 51, 100004, '佳能 EOS 70D 套机', 2.00, 2, '/uploads/2016/05/11/573318a6c8a8a.jpg', 4.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 5, 0),
	(62, 52, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(63, 53, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(64, 54, 100000, '周生生 18K金 白色黄金钻戒Belief女款 指寸12', 1.00, 1, '/uploads/2016/05/02/572767be9f2ad.jpg', 1.00, 0, 8, '3', 3, 200, 4, NULL, NULL, 1, 0),
	(65, 55, 100004, '佳能 EOS 70D 套机', 2.00, 1, '/uploads/2016/05/11/573318a6c8a8a.jpg', 2.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 5, 0),
	(66, 56, 100004, '佳能 EOS 70D 套机', 2.00, 1, '/uploads/2016/05/11/573318a6c8a8a.jpg', 2.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 5, 0),
	(67, 57, 100004, '佳能 EOS 70D 套机', 2.00, 1, '/uploads/2016/05/11/573318a6c8a8a.jpg', 2.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 5, 0),
	(68, 58, 100004, '佳能 EOS 70D 套机', 2.00, 1, '/uploads/2016/05/11/573318a6c8a8a.jpg', 2.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 5, 0),
	(73, 61, 100004, '佳能 EOS 70D 套机', 2.00, 1, '/uploads/2016/05/11/573318a6c8a8a.jpg', 2.00, 0, 22, '3', 3, 200, 1, NULL, NULL, 5, 0),
	(74, 62, 100004, '佳能 EOS 70D 套机', 2.00, 1, '/uploads/2016/05/11/573318a6c8a8a.jpg', 2.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 5, 0),
	(75, 65, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(76, 66, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(78, 68, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(79, 69, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(80, 70, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(81, 71, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(82, 72, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(83, 73, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(84, 74, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(85, 75, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(86, 76, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(87, 77, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(88, 78, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(89, 79, 100001, 'Apple iPhone6s Plus 64G 颜色随机', 890.00, 1, '/uploads/2016/05/11/5733167027cad.jpg', 890.00, 0, 23, '3', 3, 200, 1, NULL, NULL, 2, 0),
	(90, 84, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(91, 85, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(92, 86, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(93, 87, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(94, 88, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(95, 89, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(96, 90, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(97, 91, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(98, 92, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(99, 93, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(100, 94, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(101, 95, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(102, 96, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(103, 97, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(104, 98, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(105, 99, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(106, 100, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(107, 101, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(108, 102, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(109, 103, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(110, 104, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(111, 105, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(112, 106, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(113, 107, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(114, 108, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(120, 114, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 1, '/uploads/2016/05/11/573318526ca1e.jpg', 10.00, 0, 25, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(122, 116, 100002, 'Apple iMac 27 英寸配备 Retina 5K 显示屏 MK482CH/A', 10.00, 2, '/uploads/2016/05/11/573318526ca1e.jpg', 20.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 3, 0),
	(123, 116, 100001, 'Apple iPhone6s Plus 64G 颜色随机', 890.00, 1, '/uploads/2016/05/11/5733167027cad.jpg', 890.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 2, 0),
	(124, 117, 100000, '周生生 18K金 白色黄金钻戒Belief女款 指寸12', 1.00, 1, '/uploads/2016/05/02/572767be9f2ad.jpg', 1.00, 0, 8, '3', 3, 200, 4, NULL, NULL, 1, 0),
	(125, 118, 100001, 'Apple iPhone6s Plus 64G 颜色随机', 890.00, 1, '/uploads/2016/05/11/5733167027cad.jpg', 890.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 2, 0),
	(126, 119, 100001, 'Apple iPhone6s Plus 64G 颜色随机', 890.00, 1, '/uploads/2016/05/11/5733167027cad.jpg', 890.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 2, 0),
	(127, 120, 100001, 'Apple iPhone6s Plus 64G 颜色随机', 890.00, 1, '/uploads/2016/05/11/5733167027cad.jpg', 890.00, 0, 8, '3', 3, 200, 1, NULL, NULL, 2, 0);
/*!40000 ALTER TABLE `do_order_goods` ENABLE KEYS */;

-- 导出  表 domall.do_order_goods_activity 结构
CREATE TABLE IF NOT EXISTS `do_order_goods_activity` (
  `oga_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '订单商品中奖情况主键',
  `rec_id` int(11) NOT NULL COMMENT '订单商品表索引id',
  `goods_id` int(11) NOT NULL COMMENT '商品id',
  `oga_code` varchar(255) DEFAULT NULL COMMENT '中奖码',
  `oga_addtime` int(11) DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`oga_id`)
) ENGINE=InnoDB AUTO_INCREMENT=259 DEFAULT CHARSET=utf8 COMMENT='订单商品中奖情况表';

-- 正在导出表  domall.do_order_goods_activity 的数据：~116 rows (大约)
DELETE FROM `do_order_goods_activity`;
/*!40000 ALTER TABLE `do_order_goods_activity` DISABLE KEYS */;
INSERT INTO `do_order_goods_activity` (`oga_id`, `rec_id`, `goods_id`, `oga_code`, `oga_addtime`) VALUES
	(1, 1, 100000, '40515609077952008', 1462265077),
	(2, 1, 100000, '92515609077952008', 1462265077),
	(3, 1, 100000, '23515609077952008', 1462265077),
	(4, 1, 100000, '15515609077952008', 1462265077),
	(8, 2, 100000, '95515778687130008', 1462434687),
	(9, 2, 100000, '13515778687130008', 1462434687),
	(11, 3, 100000, '29515778773282008', 1462434773),
	(12, 4, 100002, '52515799663066008', 1462455663),
	(13, 5, 100000, '71515799843310008', 1462455843),
	(14, 6, 100000, '81515799941846008', 1462455941),
	(15, 7, 100000, '42515800008512008', 1462456008),
	(16, 8, 100000, '20515842984954008', 1462498984),
	(17, 9, 100001, '82515861773520008', 1462517773),
	(18, 9, 100001, '97515861773520008', 1462517773),
	(20, 10, 100000, '79515861841110008', 1462517841),
	(21, 11, 100002, '96515862840525008', 1462518840),
	(22, 11, 100002, '45515862840525008', 1462518840),
	(24, 12, 100001, '43515869960901008', 1462525960),
	(114, 27, 100002, '60516039789628008', 1462695789),
	(115, 27, 100002, '81516039789628008', 1462695789),
	(116, 27, 100002, '20516039789628008', 1462695789),
	(117, 27, 100002, '60516039789628008', 1462695789),
	(118, 27, 100002, '32516039789628008', 1462695789),
	(119, 27, 100002, '96516039789628008', 1462695789),
	(120, 27, 100002, '99516039789628008', 1462695789),
	(121, 28, 100001, '16516039789653008', 1462695789),
	(122, 29, 100002, '69516039904944008', 1462695904),
	(123, 30, 100002, '35516040960381008', 1462696960),
	(124, 31, 100002, '68516046004056008', 1462702004),
	(125, 32, 100002, '31516046496650008', 1462702496),
	(126, 33, 100002, '43516048735871008', 1462704735),
	(127, 34, 100000, '13516049139846008', 1462705139),
	(158, 37, 100001, '61516133998746008', 1462789998),
	(159, 38, 100001, '11516134279313008', 1462790279),
	(160, 39, 100004, '45516136507686008', 1462792507),
	(161, 40, 100003, '52516143929902008', 1462799929),
	(162, 41, 100002, '52516145505980008', 1462801505),
	(163, 42, 100001, '54516145872429008', 1462801872),
	(164, 43, 100003, '58516145923931008', 1462801923),
	(165, 44, 100001, '66516146228227008', 1462802228),
	(166, 45, 100004, '29516146266154008', 1462802266),
	(167, 46, 100004, '66516146339391008', 1462802339),
	(168, 47, 100001, '58516146862259008', 1462802862),
	(169, 48, 100004, '70516146960065008', 1462802960),
	(170, 49, 100001, '82516147237967008', 1462803237),
	(171, 50, 100004, '72516147768855008', 1462803768),
	(172, 51, 100004, '10516147803564008', 1462803803),
	(173, 52, 100001, '58516148774376008', 1462804774),
	(174, 53, 100000, '33516149116511008', 1462805116),
	(175, 53, 100000, '90516149116511008', 1462805116),
	(177, 54, 100004, '77516150537209008', 1462806537),
	(178, 55, 100000, '16516150692971008', 1462806692),
	(179, 56, 100001, '86516300706690008', 1462956706),
	(180, 57, 100000, '47516300810124008', 1462956810),
	(181, 58, 100004, '73516308194271008', 1462964194),
	(182, 59, 100003, '73516385830195008', 1463041830),
	(183, 59, 100003, '51516385830195008', 1463041830),
	(185, 60, 100002, '99516385830222008', 1463041830),
	(186, 61, 100004, '53516482003209008', 1463138003),
	(187, 61, 100004, '84516482003209008', 1463138003),
	(189, 62, 100002, '57516482064135008', 1463138064),
	(190, 63, 100002, '70516482153072008', 1463138153),
	(191, 64, 100000, '87516482378928008', 1463138378),
	(192, 65, 100004, '92516715326441008', 1463371326),
	(193, 66, 100004, '48516723639328008', 1463379639),
	(194, 67, 100004, '38516723675470008', 1463379675),
	(195, 68, 100004, '35516732100332008', 1463388100),
	(200, 73, 100004, '64516735937992022', 1463391937),
	(201, 74, 100004, '24516750261619008', 1463406261),
	(202, 75, 100002, '15516751442964008', 1463407442),
	(203, 76, 100002, '40516751781124008', 1463407781),
	(205, 78, 100002, '14516752833624008', 1463408833),
	(206, 79, 100002, '93516752867675008', 1463408867),
	(207, 80, 100002, '60516752934092008', 1463408934),
	(208, 81, 100002, '91516753016726008', 1463409016),
	(209, 82, 100002, '34516753738853008', 1463409738),
	(210, 83, 100002, '71516754299827008', 1463410299),
	(211, 84, 100002, '17516754343977008', 1463410343),
	(212, 85, 100002, '40516754677330008', 1463410677),
	(213, 86, 100002, '99516755076433008', 1463411076),
	(214, 87, 100002, '63516755137393008', 1463411137),
	(215, 88, 100002, '97516834001136008', 1463490001),
	(216, 89, 100001, '38516835488470023', 1463491488),
	(217, 90, 100002, '46516880022310008', 1463536022),
	(218, 91, 100002, '85516880775525008', 1463536775),
	(219, 92, 100002, '60516881065906008', 1463537065),
	(220, 93, 100002, '42516881620608008', 1463537620),
	(221, 94, 100002, '34516883948626008', 1463539948),
	(222, 95, 100002, '47516884009804008', 1463540009),
	(223, 96, 100002, '64516884952081008', 1463540952),
	(224, 97, 100002, '51516885138823008', 1463541138),
	(225, 98, 100002, '31516885877624008', 1463541877),
	(226, 99, 100002, '32516886371959008', 1463542371),
	(227, 100, 100002, '36516886972398008', 1463542972),
	(228, 101, 100002, '24516887301671008', 1463543301),
	(229, 102, 100002, '67516888265858008', 1463544265),
	(230, 103, 100002, '32516890927924008', 1463546927),
	(231, 104, 100002, '12516892114471008', 1463548114),
	(232, 105, 100002, '61516892432918008', 1463548432),
	(233, 106, 100002, '42516892500694008', 1463548500),
	(234, 107, 100002, '10516893034677008', 1463549034),
	(235, 108, 100002, '49516893206593008', 1463549206),
	(236, 109, 100002, '19516893302126008', 1463549302),
	(237, 110, 100002, '31516893388802008', 1463549388),
	(238, 111, 100002, '34516894600729008', 1463550600),
	(239, 112, 100002, '36516895427958008', 1463551427),
	(240, 113, 100002, '41516897089748008', 1463553089),
	(241, 114, 100002, '44516897415029008', 1463553415),
	(249, 120, 100002, '93517050876652025', 1463706876),
	(251, 122, 100002, '32517180727524008', 1463836727),
	(252, 122, 100002, '93517180727524008', 1463836727),
	(254, 123, 100001, '89517180727554008', 1463836727),
	(255, 124, 100000, '57517180875526008', 1463836875),
	(256, 125, 100001, '97517181182035008', 1463837182),
	(257, 126, 100001, '22517181266930008', 1463837266),
	(258, 127, 100001, '48517181337483008', 1463837337);
/*!40000 ALTER TABLE `do_order_goods_activity` ENABLE KEYS */;

-- 导出  表 domall.do_payment 结构
CREATE TABLE IF NOT EXISTS `do_payment` (
  `payment_id` tinyint(1) unsigned NOT NULL COMMENT '支付索引id',
  `payment_code` char(10) NOT NULL COMMENT '支付代码名称',
  `payment_name` char(10) NOT NULL COMMENT '支付名称',
  `payment_config` text COMMENT '支付接口配置信息',
  `payment_state` enum('0','1') NOT NULL DEFAULT '0' COMMENT '接口状态0禁用1启用',
  PRIMARY KEY (`payment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='支付方式表';

-- 正在导出表  domall.do_payment 的数据：~0 rows (大约)
DELETE FROM `do_payment`;
/*!40000 ALTER TABLE `do_payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `do_payment` ENABLE KEYS */;

-- 导出  表 domall.do_seller 结构
CREATE TABLE IF NOT EXISTS `do_seller` (
  `seller_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '卖家编号',
  `seller_name` varchar(50) NOT NULL COMMENT '卖家用户名',
  `member_id` int(10) unsigned NOT NULL COMMENT '用户编号',
  `seller_group_id` int(10) unsigned NOT NULL COMMENT '卖家组编号',
  `store_id` int(10) unsigned NOT NULL COMMENT '店铺编号',
  `is_admin` tinyint(3) unsigned NOT NULL COMMENT '是否管理员(0-不是 1-是)',
  `seller_quicklink` varchar(255) DEFAULT NULL COMMENT '卖家快捷操作',
  `last_login_time` int(10) unsigned DEFAULT NULL COMMENT '最后登录时间',
  `is_client` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否客户端用户 0-否 1-是',
  PRIMARY KEY (`seller_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='卖家用户表';

-- 正在导出表  domall.do_seller 的数据：~0 rows (大约)
DELETE FROM `do_seller`;
/*!40000 ALTER TABLE `do_seller` DISABLE KEYS */;
/*!40000 ALTER TABLE `do_seller` ENABLE KEYS */;

-- 导出  表 domall.do_seller_group 结构
CREATE TABLE IF NOT EXISTS `do_seller_group` (
  `group_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '卖家组编号',
  `group_name` varchar(50) NOT NULL COMMENT '组名',
  `limits` text NOT NULL COMMENT '权限',
  `smt_limits` text COMMENT '消息权限范围',
  `gc_limits` tinyint(3) unsigned DEFAULT '1' COMMENT '1拥有所有分类权限，0拥有部分分类权限',
  `store_id` int(10) unsigned NOT NULL COMMENT '店铺编号',
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='卖家用户组表';

-- 正在导出表  domall.do_seller_group 的数据：~0 rows (大约)
DELETE FROM `do_seller_group`;
/*!40000 ALTER TABLE `do_seller_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `do_seller_group` ENABLE KEYS */;

-- 导出  表 domall.do_setting 结构
CREATE TABLE IF NOT EXISTS `do_setting` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '配置ID',
  `title` varchar(32) NOT NULL DEFAULT '' COMMENT '配置标题',
  `name` varchar(32) NOT NULL DEFAULT '' COMMENT '配置名称',
  `value` text NOT NULL COMMENT '配置值',
  `group` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '配置分组',
  `type` varchar(16) NOT NULL DEFAULT '' COMMENT '配置类型',
  `options` varchar(255) NOT NULL DEFAULT '' COMMENT '配置额外值',
  `tip` varchar(100) NOT NULL DEFAULT '' COMMENT '配置说明',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `sort` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COMMENT='系统配置表';

-- 正在导出表  domall.do_setting 的数据：11 rows
DELETE FROM `do_setting`;
/*!40000 ALTER TABLE `do_setting` DISABLE KEYS */;
INSERT INTO `do_setting` (`id`, `title`, `name`, `value`, `group`, `type`, `options`, `tip`, `create_time`, `update_time`, `sort`, `status`) VALUES
	(1, '站点开关', 'toggle_site', '1', 1, 'enum', '0:关闭\n1:开启', '站点关闭后将不能访问', 1378898976, 1458231934, 2, 1),
	(2, '网站标题', 'site_title', 'DoMall', 1, 'text', '', '网站标题前台显示标题', 1378898976, 1458231875, 1, 1),
	(11, '文件上传大小', 'upload_file_size', '10', 2, 'num', '', '文件上传大小单位：MB', 1428681031, 1458232004, 1, 1),
	(12, '图片上传大小', 'upload_image_size', '2', 2, 'num', '', '图片上传大小单位：MB', 1428681071, 1458232023, 2, 1),
	(14, '分页数量', 'admin_page_rows', '10', 2, 'num', '', '分页时每页的记录数', 1434019462, 1458232103, 4, 1),
	(16, '开发模式', 'develop_mode', '0', 2, 'enum', '1:开启\n0:关闭', '开发模式下会显示菜单管理、配置管理、数据字典等开发者工具', 1432393583, 1458232229, 1, 1),
	(17, '是否显示页面Trace', 'show_page_trace', '0', 2, 'num', '0:关闭\n1:开启', '是否显示页面Trace信息', 1387165685, 1458232276, 2, 1),
	(25, '配置分组', 'config_group_list', '0:系统\n1:基本\n2:开发', 0, 'array', '', '系统分组', 0, 1458231584, 1, 1),
	(28, '配置类型', 'config_type_list', 'num:数字\ntext:文本\narray:数组\nenum:枚举', 0, 'array', '', '', 1458231729, 0, 2, 1),
	(19, 'URL模式', 'url_model', '3', 2, 'num', '0:普通模式\n1:PATHINFO模式\n2:REWRITE模式\n3:兼容模式', '', 1438423248, 1458232256, 1, 1),
	(20, '静态文件独立域名', 'STATIC_DOMAIN', '', 2, 'text', '', '静态文件独立域名一般用于在用户无感知的情况下平和的将网站图片自动存储到腾讯万象优图、又拍云等第三方服务。', 1438564784, 1438564784, 3, 1);
/*!40000 ALTER TABLE `do_setting` ENABLE KEYS */;

-- 导出  表 domall.do_store 结构
CREATE TABLE IF NOT EXISTS `do_store` (
  `store_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '店铺索引id',
  `store_name` varchar(50) NOT NULL COMMENT '店铺名称',
  `grade_id` int(11) NOT NULL COMMENT '店铺等级',
  `member_id` int(11) NOT NULL COMMENT '会员id',
  `member_name` varchar(50) NOT NULL COMMENT '会员名称',
  `seller_name` varchar(50) DEFAULT NULL COMMENT '店主卖家用户名',
  `sc_id` int(11) NOT NULL DEFAULT '0' COMMENT '店铺分类',
  `store_company_name` varchar(50) DEFAULT NULL COMMENT '店铺公司名称',
  `province_id` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '店铺所在省份ID',
  `area_info` varchar(100) NOT NULL DEFAULT '' COMMENT '地区内容，冗余数据',
  `store_address` varchar(100) NOT NULL DEFAULT '' COMMENT '详细地区',
  `store_zip` varchar(10) NOT NULL DEFAULT '' COMMENT '邮政编码',
  `store_state` tinyint(1) NOT NULL DEFAULT '2' COMMENT '店铺状态，0关闭，1开启，2审核中',
  `store_close_info` varchar(255) DEFAULT NULL COMMENT '店铺关闭原因',
  `store_sort` int(11) NOT NULL DEFAULT '0' COMMENT '店铺排序',
  `store_time` varchar(10) NOT NULL COMMENT '店铺时间',
  `store_end_time` varchar(10) DEFAULT NULL COMMENT '店铺关闭时间',
  `store_label` varchar(255) DEFAULT NULL COMMENT '店铺logo',
  `store_banner` varchar(255) DEFAULT NULL COMMENT '店铺横幅',
  `store_avatar` varchar(150) DEFAULT NULL COMMENT '店铺头像',
  `store_keywords` varchar(255) NOT NULL DEFAULT '' COMMENT '店铺seo关键字',
  `store_description` varchar(255) NOT NULL DEFAULT '' COMMENT '店铺seo描述',
  `store_qq` varchar(50) DEFAULT NULL COMMENT 'QQ',
  `store_ww` varchar(50) DEFAULT NULL COMMENT '阿里旺旺',
  `store_phone` varchar(20) DEFAULT NULL COMMENT '商家电话',
  `store_zy` text COMMENT '主营商品',
  `store_domain` varchar(50) DEFAULT NULL COMMENT '店铺二级域名',
  `store_domain_times` tinyint(1) unsigned DEFAULT '0' COMMENT '二级域名修改次数',
  `store_recommend` tinyint(1) NOT NULL DEFAULT '0' COMMENT '推荐，0为否，1为是，默认为0',
  `store_theme` varchar(50) NOT NULL DEFAULT 'default' COMMENT '店铺当前主题',
  `store_credit` int(10) NOT NULL DEFAULT '0' COMMENT '店铺信用',
  `store_desccredit` float NOT NULL DEFAULT '0' COMMENT '描述相符度分数',
  `store_servicecredit` float NOT NULL DEFAULT '0' COMMENT '服务态度分数',
  `store_deliverycredit` float NOT NULL DEFAULT '0' COMMENT '发货速度分数',
  `store_collect` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '店铺收藏数量',
  `store_slide` text COMMENT '店铺幻灯片',
  `store_slide_url` text COMMENT '店铺幻灯片链接',
  `store_stamp` varchar(200) DEFAULT NULL COMMENT '店铺印章',
  `store_printdesc` varchar(500) DEFAULT NULL COMMENT '打印订单页面下方说明文字',
  `store_sales` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '店铺销量',
  `store_presales` text COMMENT '售前客服',
  `store_aftersales` text COMMENT '售后客服',
  `store_workingtime` varchar(100) DEFAULT NULL COMMENT '工作时间',
  `store_free_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '超出该金额免运费，大于0才表示该值有效',
  `store_decoration_switch` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '店铺装修开关(0-关闭 装修编号-开启)',
  `store_decoration_only` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '开启店铺装修时，仅显示店铺装修(1-是 0-否',
  `store_decoration_image_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '店铺装修相册图片数量',
  `is_own_shop` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否自营店铺 1是 0否',
  `bind_all_gc` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '自营店是否绑定全部分类 0否1是',
  `store_vrcode_prefix` char(3) DEFAULT NULL COMMENT '商家兑换码前缀',
  `mb_title_img` varchar(150) DEFAULT NULL COMMENT '手机店铺 页头背景图',
  `mb_sliders` text COMMENT '手机店铺 轮播图链接地址',
  `left_bar_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '店铺商品页面左侧显示类型 1默认 2商城相关分类品牌商品推荐',
  `deliver_region` varchar(50) DEFAULT NULL COMMENT '店铺默认配送区域',
  PRIMARY KEY (`store_id`),
  KEY `store_name` (`store_name`),
  KEY `sc_id` (`sc_id`),
  KEY `store_state` (`store_state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='店铺数据表';

-- 正在导出表  domall.do_store 的数据：~0 rows (大约)
DELETE FROM `do_store`;
/*!40000 ALTER TABLE `do_store` DISABLE KEYS */;
/*!40000 ALTER TABLE `do_store` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
