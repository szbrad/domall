<?php

/**
 * 根据配置类型解析配置
 * @param  integer $type  配置类型
 * @param  string  $value 配置值
 * @return mixed
 * @author doogie <461960962@qq.com>
 */
function parse($type, $value){
    switch ($type) {
        case 'enum':
        case 'array': //解析数组
            $array = preg_split('/[,;\r\n]+/', trim($value, ",;\r\n"));
            if(strpos($value,':')){
                $value  = [];
                foreach ($array as $val) {
                    list($k, $v) = explode(':', $val);
                    $value[$k]   = $v;
                }
            }else{
                $value = $array;
            }
            break;
    }
    return $value;
}

/**
 * 友好显示状态
 * @param int $status
 * @return string
 * @author doogie <461960962@qq.com>
 */
function html_status($status = 1){
    return $status == 1 ? '<i class="fa fa-check text-success"></i>' : '<i class="fa fa-ban text-danger"></i>';
}

/**
 * 获取所有数据并转换成一维数组
 * @author doogie <461960962@qq.com>
 */
function select_list_as_tree($model, $map = null, $extra = null, $key = 'id') {
    //获取列表
    $con['status'] = array('eq', 1);
    if ($map) {
        $con = array_merge($con, $map);
    }
    $model = D($model);
    if (in_array('sort', $model->getFields())) {
        $list = $model->where($con)->order('sort asc, id asc')->select();
    } else {
        $list = $model->where($con)->order('id asc')->select();
    }

    //转换成树状列表(非严格模式)
    $tree = new \app\common\library\Tree();
    $list = $tree->toFormatTree($list, 'title', 'id', 'pid', 0, false);

    if ($extra) {
        $result[0] = $extra;
    }

    //转换成一维数组
    foreach ($list as $val) {
        $result[$val[$key]] = $val['title_show'];
    }
    return $result;
}