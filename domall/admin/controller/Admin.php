<?php
// +----------------------------------------------------------------------
// | ProjectName : Wuliangye
// +----------------------------------------------------------------------
// | Description :  用户管理
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2016 http://www.idowe.com All rights reserved.
// +----------------------------------------------------------------------
// | Authors : Johhny <chenjf@idowe.com>  Date : 2016-10-25
// +----------------------------------------------------------------------
namespace app\admin\controller;

use think\Db;
use think\Request;
use think\Response;

class Admin extends Base{
    public function index(Request $request){
        if($request->isGet()){
            return $this->fetch();
        }

        $adminList = Db::view('admin','*')
                    ->where('admin.status = 0')
                    ->select();
        foreach($adminList as $key=>$adminInfo){
            $adminList[$key]['admin_login_time'] = date('Y-m-d H:i:s',$adminInfo['admin_login_time']);
            $role_info = model('AdminRole')->where('role_id',$adminInfo['role_id'])->find();
            $adminList[$key]['role_name'] = $role_info['role_name'];
            $adminList[$key]['is_super'] = $adminInfo['admin_is_super']==1?'是':'否';
            $adminList[$key]['is_member'] = $adminInfo['member_id']>0?'是':'否';
            if($adminList[$key]['is_member'] == '是'){
                $member_info = model('member')->get($adminInfo['member_id']);
                $adminList[$key]['member_name'] = $member_info['member_name'];
            }
        }

        return json([
            "rows"  =>  $adminList,
            "total" =>  count($adminList)
        ]);
    }

    /**
     * 编辑和新增
     * @param Request $request
     * @return Response|void
     */
    public function editAdmin(Request $request){
        if($request->isPost()){
            if( $id = $request->post("admin_id")){
                $admin_info = $request->post();
                $admin_info['create_time'] = time();
                $admin_info['admin_login_time'] = time();
                if(!empty($admin_info['admin_password'])){
                    $admin_info['admin_password'] = md5($admin_info['admin_password']);
                }else{
                    unset($admin_info['admin_password']);
                }
                $state  =  model('Admin')->validate('Admin.edit')->save($admin_info,[ "admin_id" => $id]);
            }else{
                $admin_info = $request->post();
                $admin_info['create_time'] = time();
                $admin_info['admin_login_time'] = time();
                $admin_info['identifying']      = model('Admin')->getIdentifying();
                if(!empty($admin_info['admin_password'])){
                    $admin_info['admin_password'] = md5($admin_info['admin_password']);
                }else{
                    unset($admin_info['admin_password']);
                }
                $state  =  model('Admin')->validate('Admin.add')->save($admin_info);
            }

            return $state ? $this->success("保存成功!") : $this->error(model('Admin')->getError());
        }else{
            $id = $request->param('id');
            $adminItem =  model('Admin')->where('admin_id',$id)->find();
            $this->assign('adminItem',$adminItem);
            $roleList = model('AdminRole')->select();
            $this->assign('roleList',$roleList);

            $memberList = model('Member')->where('ifnull(admin_id,0) = 0')->select();
            $this->assign('memberList',$memberList);
            $this->assign([
                'meta_title' => '编辑用户',
                'post_url' => url('edit'),
            ]);
            return new Response($this->fetch() , 200);
        }
    }

    /**
     * 删除功能
     * @param Request $request
     */
    public function delete(Request $request){
        $id = $request->param('id');
        if( !empty($id)){
            $admin_info = model('Admin')->where('admin_id',$id)->find();
            if($admin_info['admin_is_super'] == 1){
                return $this->error('系统超级管理员，不允许删除!');
            }
            $admin_info->delete();
        }
        return $this->success('删除成功!');
    }
}