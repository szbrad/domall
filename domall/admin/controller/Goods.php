<?php
// +----------------------------------------------------------------------
// | ProjectName : domall
// +----------------------------------------------------------------------
// | Description :  商品管理
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2016 http://www.idowe.com All rights reserved.
// +----------------------------------------------------------------------
// | Authors : Johhny <chenjf@idowe.com>  Date : 2016-02-15
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\common\model;

class Goods extends Base{
    public function index(){

        $goods = model\Goods::where(array('id',array('neq',0)))->limit(15);

        $this->assign('goods',$goods);
        return $this->display();
    }
}