<?php
// +----------------------------------------------------------------------
// | ProjectName : Wuliangye
// +----------------------------------------------------------------------
// | Description :  
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2016 http://www.idowe.com All rights reserved.
// +----------------------------------------------------------------------
// | Authors : Johhny <chenjf@idowe.com>  Date : 2016-10-26
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\common\library\Tree;
use think\Request;
use think\Response;

class AdminRole extends Base{
    public function __construct() {
        parent::__construct();
    }

    /**
     * 首页
     * @param Request $request
     * @return string|\think\response\Json
     */
    public function index(Request $request){
        if($request->isGet()){
            return $this->fetch();
        }

        $roleList =  model('AdminRole')->select();
        return json([
            "rows"  =>  $roleList,
            "total" =>  count($roleList)
        ]);
    }

    /**
     * 编辑和新增
     * @param Request $request
     * @return Response|void
     */
    public function editRole(Request $request){
        if($request->isPost()){
            $role_group = $request->param('role_group');

            $role_group = json_decode($role_group,true);

            if( $id = $request->param("role_id")){
                $role_info = $request->post();
                $role_info['create_time'] = time();
                $role_info['role_group'] = serialize($role_group);
                $state  =  model('AdminRole')->validate('AdminRole.edit')->save($role_info,[ "role_id" => $id]);
            }else{
                $role_info = $request->post();
                $role_info['create_time'] = time();
                $role_info['role_group'] = serialize($role_group);
                $state  =  model('AdminRole')->validate('AdminRole.add')->save($role_info);
            }

            return $state ? $this->success("保存成功!") : $this->error(model('AdminRole')->getError());
        }else{
            $id = $request->param('id');
            $roleItem =  model('AdminRole')->where('role_id',$id)->find();
            $roleItem['role_group'] = unserialize($roleItem['role_group']);
            $this->assign('roleItem',$roleItem);

            $authList =  model('AdminAuth')->order('sort asc')->select();

            $tree = new Tree();
            $auth_list = $tree->toFormatTree($authList);

            $this->assign([
                'meta_title' => '编辑角色',
                'post_url' => url('edit'),
                'auth_list'=>$auth_list
            ]);
            return new Response($this->fetch() , 200);
        }
    }

    /**
     * 删除功能
     * @param Request $request
     */
    public function delete(Request $request){
        $id = $request->param('id');
        if( !empty($id)){
            $role_info = model('AdminRole')->where('role_id',$id)->find();
            $role_info->delete();
        }
        return $this->success('删除成功!');
    }
}