<?php
// +----------------------------------------------------------------------
// | ProjectName : domall
// +----------------------------------------------------------------------
// | Description :  登录类
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2016 http://www.idowe.com All rights reserved.
// +----------------------------------------------------------------------
// | Authors : Johhny <chenjf@idowe.com>  Date : 2016-02-03
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\common\model\Admin;
use app\common\model\Gadmin;
use think\Request;

class Login extends Base{

    /**
     * 构造函数
     * Login constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 登录页面
     * @author Johhny <chenjf@idowe.com>
     */
    public function index(Request $request){
        if($request->isPost()){
            $username = $_POST['username'];
            $password = $_POST['password'];

            $admin_info = Admin::where(array('admin_name'=>$username))->find();
            if(!empty($admin_info) && $admin_info['admin_password'] == md5($password)){
                if($admin_info['admin_is_super'] == 1){
                    $admin_info['role_name'] = '超级管理员';
                }else if($admin_info['role_id'] > 0) {
                    $role_info = model('AdminRole')->where(array('role_id'=>$admin_info['role_id']))->find();
                    $admin_info['role_name'] = $role_info['role_name'];
                } else {
                    $admin_info['role_name'] = '无权限用户';
                }
                $array = array();
                $this->systemSetKey($admin_info, $admin_info['admin_avatar'], true);
                $update_info    = array(
                    'admin_id'=>$admin_info['admin_id'],
                    'admin_login_num'=>($admin_info['admin_login_num']+1),
                    'admin_login_time'=>time()
                );
                Admin::update($update_info);
                $this->redirect('/admin/index');

            }else{
                return $this->error('用户名或密码错误');
            }

        }else{
            return $this->display();
        }
    }

    /**
     * 退出操作
     * @author Doogie <461960962@qq.com>
     */
    public function logout(){
        $this->systemSetKey(null);
        return $this->success('安全退出', url('Login/index'));
    }

}