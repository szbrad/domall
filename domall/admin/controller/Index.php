<?php
// +----------------------------------------------------------------------
// | ProjectName : domall
// +----------------------------------------------------------------------
// | Description :  平台后台首页
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2016 http://www.idowe.com All rights reserved.
// +----------------------------------------------------------------------
// | Authors : Johhny <chenjf@idowe.com>  Date : 2016-02-03
// +----------------------------------------------------------------------
namespace app\admin\controller;

use think\Db;

class Index extends Base{

    /**
     * 后台首页
     * @author Johhny <chenjf@idowe.com>
     * @return string
     */
    public function index(){

        return $this->fetch();
    }

    /**
     * 首页
     * @return string
     */
    public function home(){
        return $this->fetch();
    }


}