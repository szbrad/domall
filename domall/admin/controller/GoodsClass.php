<?php
// +----------------------------------------------------------------------
// | ProjectName : domall
// +----------------------------------------------------------------------
// | Description :  商品分类管理
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2016 http://www.idowe.com All rights reserved.
// +----------------------------------------------------------------------
// | Authors : Johhny <chenjf@idowe.com>  Date : 2016-03-01
// +----------------------------------------------------------------------
namespace app\admin\controller;

class GoodsClass extends Base{
    public function index(){
        // 商品分类
        $goods_class = M('goods_class')->where(array('gc_parent_id'=>0))->select();

        $this->assign('goods_class', $goods_class);
        return $this->display();
    }

    /**
     * 新增和编辑商品分类
     * @author Johhny <chenjf@idowe.com>
     */
    public function edit(){

        if(IS_POST){
            $data['gc_name'] = $_POST['gc_name'];
            $data['show_type'] = $_POST['show_type'];
            $data['type_id'] = $_POST['type_id'];
            $data['type_name'] = $_POST["type_name"];
            $data['gc_sort'] = $_POST['gc_sort'];

            $goods_info = M('goods_class')->where(array('gc_name'=>$data['gc_name']))->find();
            if($goods_info){
                return $this->error('分类名称已存在');
            }else{
                M('goods_class')->add($data);
            }

            return $this->success('保存成功', '', url('admin/index'));
        }else{
            // 商品分类
            $goods_class = M('goods_class')->select();

            $this->assign('goods_class_list', $goods_class);

            return $this->display();
        }
    }

    /**
     * 删除商品分类
     * @author Johhny <chenjf@idowe.com>
     */
    public function delete(){
        $id = $_POST['id'];
        $goods_info = M('goods')->where(array('gc_id'=>$id))->find();
        if($goods_info){
            return $this->error('该分类已被商品关联，不允许删除！');
        }else{
            $result = M('goods_class')->where(array('gc_id'=>$id))->delete();
            if($result){
                return $this->success('删除成功！');
            }
        }
    }
}