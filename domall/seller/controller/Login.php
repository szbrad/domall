<?php
// +----------------------------------------------------------------------
// | ProjectName : domall
// +----------------------------------------------------------------------
// | Description :  商家登录
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2016 http://www.idowe.com All rights reserved.
// +----------------------------------------------------------------------
// | Authors : Johhny <chenjf@idowe.com>  Date : 2016-02-19
// +----------------------------------------------------------------------
namespace app\seller\controller;

class Login extends Base{

    /**
     * 构造函数
     * Login constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 登录页面
     * @author Johhny <chenjf@idowe.com>
     */
    public function index(){
        if(IS_POST){
            $seller_name = $_POST['username'];
            $password = $_POST['password'];

            $seller_info = M('seller')->where(array('seller_name'=>$seller_name))->find();
            if($seller_info){

                $member_info = M('member')->where(
                    array(
                        'member_id'=>$seller_info['member_id'],
                        'member_passwd' => md5($password)
                    )
                )->find();

                if($member_info){
                    // 更新卖家登陆时间
                    M('seller')->where(array('seller_id' => $seller_info['seller_id']))->save(array('last_login_time' => time()));

                    $seller_group_info = M('seller_group')->where(array('group_id' => $seller_info['seller_group_id']))->find();

                    $store_info = D('store')->getStoreInfoByID($seller_info['store_id']);

                    $data['is_login'] = '1';
                    $data['member_id'] = $member_info['member_id'];
                    $data['member_name'] = $member_info['member_name'];
                    $data['member_email'] = $member_info['member_email'];
                    $data['is_buy'] = $member_info['is_buy'];
                    $data['avatar'] = $member_info['member_avatar'];

                    $data['grade_id'] = $store_info['grade_id'];
                    $data['seller_id'] = $seller_info['seller_id'];
                    $data['seller_name'] = $seller_info['seller_name'];
                    $data['seller_is_admin'] = intval($seller_info['is_admin']);
                    $data['store_id'] = intval($seller_info['store_id']);
                    $data['store_name'] = $store_info['store_name'];
                    $data['store_avatar'] = $store_info['store_avatar'];
                    $data['is_own_shop'] = (bool) $store_info['is_own_shop'];
                    $data['bind_all_gc'] = (bool) $store_info['bind_all_gc'];
                    $data['seller_limits'] = explode(',', $seller_group_info['limits']);
                    $data['seller_group_id'] = $seller_info['seller_group_id'];
                    $data['seller_gc_limits'] = $seller_group_info['gc_limits'];
                    if($seller_info['is_admin']) {
                        $data['seller_group_name'] = '管理员';
                        $data['seller_smt_limits'] = false;
                    } else {
                        $data['seller_group_name'] = $seller_group_info['group_name'];
                        $data['seller_smt_limits'] = explode(',', $seller_group_info['smt_limits']);
                    }
                    if(!$seller_info['last_login_time']) {
                        $seller_info['last_login_time'] = time();
                    }
                    $data['seller_last_login_time'] = date('Y-m-d H:i', $seller_info['last_login_time']);
                    $seller_menu = $this->getSellerMenuList($seller_info['is_admin'], explode(',', $seller_group_info['limits']));
                    $data['seller_menu'] = $seller_menu['seller_menu'];
                    $data['seller_function_list'] = $seller_menu['seller_function_list'];
                    if(!empty($seller_info['seller_quicklink'])) {
                        $quicklink_array = explode(',', $seller_info['seller_quicklink']);
                        foreach ($quicklink_array as $value) {
                            $data['seller_quicklink'][$value] = $value ;
                        }
                    }

                    $this->sellerSetKey($data);

                    $this->redirect('/seller/index');
                }else{
                    return $this->error('用户名或密码错误');
                }
            }else{
                return $this->error('用户名或密码错误');
            }

        }else{
            return $this->display();
        }
    }

    /**
     * 退出操作
     * @author Doogie <461960962@qq.com>
     */
    public function logout(){
        session('seller_key',null);
        $this->success('安全退出', U('login/index'));
    }
}