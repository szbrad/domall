<?php
// +----------------------------------------------------------------------
// | ProjectName : domall
// +----------------------------------------------------------------------
// | Description :  商家后台
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2016 http://www.idowe.com All rights reserved.
// +----------------------------------------------------------------------
// | Authors : Johhny <chenjf@idowe.com>  Date : 2016-02-19
// +----------------------------------------------------------------------
namespace app\seller\controller;

class Index extends Base{
    public function index(){
        return $this->display();
    }
}