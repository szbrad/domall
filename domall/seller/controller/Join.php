<?php
// +----------------------------------------------------------------------
// | ProjectName : domall
// +----------------------------------------------------------------------
// | Description :  商家入驻
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2016 http://www.idowe.com All rights reserved.
// +----------------------------------------------------------------------
// | Authors : Johhny <chenjf@idowe.com>  Date : 2016-02-19
// +----------------------------------------------------------------------
namespace app\seller\controller;

class Join extends Base{

    /**
     * 入驻首页
     * @author Johhny <chenjf@idowe.com>
     */
    public function index(){
        if(IS_POST){
            $seller_name = $_POST['username'];
            $password = $_POST['password'];
            $store_company_name = $_POST['store_company_name'];
            $store_name = $_POST['store_name'];

            $seller_info = M('seller')->where(array('seller_name'=>$seller_name))->find();
            if(!$seller_info){
                $array = array();
                $array['member_name']   = $seller_name;
                $array['member_passwd'] = md5($password);
                $member_info = M('member')->where($array)->find();
                if($member_info){
                    $shop_array['member_id']    = $member_info['member_id'];
                    $shop_array['member_name']  = $member_info['member_name'];
                    $shop_array['seller_name'] = $member_info['member_name'];
                    $shop_array['grade_id']     = '0';
                    $shop_array['store_name']   = $store_name;
                    $shop_array['sc_id']        = '0';
                    $shop_array['store_company_name'] = $store_company_name;
                    $shop_array['province_id']  = '0';
                    $shop_array['area_info']    = '0';
                    $shop_array['store_address']= '0';
                    $shop_array['store_zip']    = '';
                    $shop_array['store_zy']     = '';
                    $shop_array['store_state']  = 1;
                    $shop_array['store_time']   = time();
                    $shop_array['store_end_time'] = strtotime(date('Y-m-d 23:59:59', strtotime('+1 day'))." +1 year");

                    $store_id = M('store')->add($shop_array);
                    if($store_id) {
                        //写入商家账号
                        $seller_array = array();
                        $seller_array['seller_name'] = $member_info['member_name'];
                        $seller_array['member_id'] = $member_info['member_id'];
                        $seller_array['seller_group_id'] = 0;
                        $seller_array['store_id'] = $store_id;
                        $seller_array['is_admin'] = 1;
                        $state = M('seller')->add($seller_array);
                        if($state){
                            return $this->success('店铺开店成功！',url('seller/login'));
                        }else{
                            return $this->error('店铺开店失败！');
                        }
                    }
                }else{
                    return $this->error('未找到用户帐号！');
                }
            }else{
                return $this->error('商家已入驻！');
            }
        }else{
            return $this->display();
        }

    }
}