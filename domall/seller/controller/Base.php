<?php
// +----------------------------------------------------------------------
// | ProjectName : domall
// +----------------------------------------------------------------------
// | Description :  商家基础类
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2016 http://www.idowe.com All rights reserved.
// +----------------------------------------------------------------------
// | Authors : Johhny <chenjf@idowe.com>  Date : 2016-02-19
// +----------------------------------------------------------------------
namespace app\seller\controller;


use think\Controller;
use think\View;

class Base extends Controller{
    /**
     * 商家资料 name id group
     */
    protected $seller_info;

    /**
     * 权限内容
     */
    protected $permission;

    /**
     * 菜单
     */
    protected $menu;

    /**
     * 常用菜单
     */
    protected $quick_link;

    /**
     * 构造函数
     * Base constructor.
     */
    public function __construct() {
        parent::__construct();

        /**
         * 验证用户是否登录
         * $admin_info 管理员资料 name id
         */
        $this->seller_info = $this->sellerLogin();
        $this->assign('seller_info',$this->seller_info);
    }

    /**
     * 商家登录验证，并判断是否已经登录，
     * 登录后则自动跳转商家后台首页
     * @param
     * @return array 数组类型的返回结果
     */
    protected final function sellerLogin() {
        $seller = unserialize(session('seller_key'));
        if ((empty($seller['seller_name']) || empty($seller['seller_id']))){
            if(CONTROLLER_NAME != 'login' && CONTROLLER_NAME != 'join'){
                $this->redirect('/seller/login');
            }
        }else {
            if(CONTROLLER_NAME == 'login'){
                $this->redirect('/seller/index');
            }else{
                $this->sellerSetKey($seller);
            }
        }
        return $seller;
    }

    /**
     * 重写fetch 方便管理视图 主题风格
     * @author Johhny <chenjf@idowe.com>
     * @param string $template
     * @param array $vars
     * @param array $cache
     * @param bool $renderContent
     * @return string
     * @throws \think\Exception
     */
    public function display($template = '', $vars = [], $cache = [], $renderContent = false){
//        $view = new View();
//        $view->assign($this->view);
        $this->view->theme(C('seller_theme'));
        $this->view->config('view_suffix',C('seller_view_suffix'));
        $this->view->config('view_depr',C('seller_view_depr'));
        return $this->view->fetch($template, $vars, $cache, $renderContent);
    }

    /**
     * 重写fetch 方便管理视图 主题风格
     * @author Johhny <chenjf@idowe.com>
     * @param string $template
     * @param array $vars
     * @param array $cache
     * @param bool $renderContent
     * @return string
     * @throws \think\Exception
     */
    public function fetch($template = '', $vars = [], $cache = [], $renderContent = false){
        return $this->display($template,$vars,$cache,$renderContent);
    }

    /**
     * 商家后台 商家登录后 将会员验证内容写入对应cookie中
     *
     * @param string $seller 商户信息
     */
    protected final function sellerSetKey($seller) {
        session('seller_key',serialize($seller));
    }

    /**
     * 取得IP
     *
     *
     * @return string 字符串类型的返回结果
     */
    function getIp(){
        $ip = $_SERVER['REMOTE_ADDR'];
        return preg_match('/^\d[\d.]+\d$/', $ip) ? $ip : '';
    }

    /**
     * 获取商家菜单列表
     * @author Johhny <chenjf@idowe.com>
     * @param $is_admin
     * @param $limits
     * @return array
     */
    protected function getSellerMenuList($is_admin, $limits) {
        $seller_menu = array();
        if (intval($is_admin) !== 1) {
            $menu_list = $this->_getMenuList();
            foreach ($menu_list as $key => $value) {
                foreach ($value['child'] as $child_key => $child_value) {
                    if (!in_array($child_value['action'], $limits)) {
                        unset($menu_list[$key]['child'][$child_key]);
                    }
                }

                if(count($menu_list[$key]['child']) > 0) {
                    $seller_menu[$key] = $menu_list[$key];
                }
            }
        } else {
            $seller_menu = $this->_getMenuList();
        }
        $seller_function_list = $this->_getSellerFunctionList($seller_menu);
        return array('seller_menu' => $seller_menu, 'seller_function_list' => $seller_function_list);
    }

    private function _getMenuList() {
        $menu_list = array(
            'goods' => array('name' => '商品', 'child' => array(
                array('name' => '商品发布', 'action'=>'store_goods_add', 'op'=>'index'),
                array('name' => 'CSV导入', 'action'=>'taobao_import', 'op'=>'index'),
                array('name' => '出售中的商品', 'action'=>'store_goods_online', 'op'=>'index'),
                array('name' => '仓库中的商品', 'action'=>'store_goods_offline', 'op'=>'index'),
                array('name' => '预约/到货通知', 'action' => 'store_appoint', 'op' => 'index'),
                array('name' => '关联版式', 'action'=>'store_plate', 'op'=>'index'),
                array('name' => '商品规格', 'action' => 'store_spec', 'op' => 'index'),
                array('name' => '图片空间', 'action'=>'store_album', 'op'=>'album_cate'),
            )),
            'order' => array('name' => '订单物流', 'child' => array(
                array('name' => '实物交易订单', 'action'=>'store_order', 'op'=>'index'),
                array('name' => '虚拟兑码订单', 'action'=>'store_vr_order', 'op'=>'index'),
                array('name' => '发货', 'action'=>'store_deliver', 'op'=>'index'),
                array('name' => '发货设置', 'action'=>'store_deliver_set', 'op'=>'daddress_list'),
                array('name' => '运单模板', 'action'=>'store_waybill', 'op'=>'waybill_manage'),
                array('name' => '评价管理', 'action'=>'store_evaluate', 'op'=>'list'),
                array('name' => '物流工具', 'action'=>'store_transport', 'op'=>'index'),
            )),
            'promotion' => array('name' => '促销', 'child' => array(
                array('name' => '团购管理', 'action'=>'store_groupbuy', 'op'=>'index'),
                array('name' => '加价购', 'action'=>'store_promotion_cou', 'op'=>'cou_list'),
                array('name' => '限时折扣', 'action'=>'store_promotion_xianshi', 'op'=>'xianshi_list'),
                array('name' => '满即送', 'action'=>'store_promotion_mansong', 'op'=>'mansong_list'),
                array('name' => '优惠套装', 'action'=>'store_promotion_bundling', 'op'=>'bundling_list'),
                array('name' => '推荐展位', 'action' => 'store_promotion_booth', 'op' => 'booth_goods_list'),
                array('name' => '预售商品', 'action' => 'store_promotion_book', 'op' => 'index'),
                array('name' => 'F码商品', 'action' => 'store_promotion_fcode', 'op' => 'index'),
                array('name' => '推荐组合', 'action' => 'store_promotion_combo', 'op' => 'index'),
                array('name' => '手机专享', 'action' => 'store_promotion_sole', 'op' => 'index'),
                array('name' => '代金券管理', 'action'=>'store_voucher', 'op'=>'templatelist'),
                array('name' => '活动管理', 'action'=>'store_activity', 'op'=>'store_activity'),
            )),
            'store' => array('name' => '店铺', 'child' => array(
                array('name' => '店铺设置', 'action'=>'store_setting', 'op'=>'store_setting'),
                array('name' => '店铺装修', 'action'=>'store_decoration', 'op'=>'decoration_setting'),
                array('name' => '店铺导航', 'action'=>'store_navigation', 'op'=>'navigation_list'),
                array('name' => '店铺动态', 'action'=>'store_sns', 'op'=>'index'),
                array('name' => '店铺信息', 'action'=>'store_info', 'op'=>'bind_class'),
                array('name' => '店铺分类', 'action'=>'store_goods_class', 'op'=>'index'),
                array('name' => '品牌申请', 'action'=>'store_brand', 'op'=>'brand_list'),
                array('name' => '供货商', 'action'=>'store_supplier', 'op'=>'sup_list'),
                array('name' => '实体店铺', 'action'=>'store_map', 'op'=>'index'),
                array('name' => '消费者保障服务', 'action'=>'store_contract', 'op'=>'index'),
            )),
            'consult' => array('name' => '售后服务', 'child' => array(
                array('name' => '咨询管理', 'action'=>'store_consult', 'op'=>'consult_list'),
                array('name' => '投诉管理', 'action'=>'store_complain', 'op'=>'list'),
                array('name' => '退款记录', 'action'=>'store_refund', 'op'=>'index'),
                array('name' => '退货记录', 'action'=>'store_return', 'op'=>'index'),
            )),
            'statistics' => array('name' => '统计结算', 'child' => array(
                array('name' => '店铺概况', 'action'=>'statistics_general', 'op'=>'general'),
                array('name' => '商品分析', 'action'=>'statistics_goods', 'op'=>'goodslist'),
                array('name' => '运营报告', 'action'=>'statistics_sale', 'op'=>'sale'),
                array('name' => '行业分析', 'action'=>'statistics_industry', 'op'=>'hot'),
                array('name' => '流量统计', 'action'=>'statistics_flow', 'op'=>'storeflow'),
                array('name' => '实物结算', 'action'=>'store_bill', 'op'=>'index'),
                array('name' => '虚拟结算', 'action'=>'store_vr_bill', 'op'=>'index'),
            )),
            'message' => array('name' => '客服消息', 'child' => array(
                array('name' => '客服设置', 'action'=>'store_callcenter', 'op'=>'index'),
                array('name' => '系统消息', 'action'=>'store_msg', 'op'=>'index'),
                array('name' => '聊天记录查询', 'action'=>'store_im', 'op'=>'index'),
            )),
            'account' => array('name' => '账号', 'child' => array(
                array('name' => '账号列表', 'action'=>'store_account', 'op'=>'account_list'),
                array('name' => '账号组', 'action'=>'store_account_group', 'op'=>'group_list'),
                array('name' => '账号日志', 'action'=>'seller_log', 'op'=>'log_list'),
                array('name' => '店铺消费', 'action'=>'store_cost', 'op'=>'cost_list'),
                array('name' => '门店账号', 'action'=>'store_chain', 'op'=>'index'),
            ))
        );
        return $menu_list;
    }

    private function _getSellerFunctionList($menu_list) {
        $format_menu = array();
        foreach ($menu_list as $key => $menu_value) {
            foreach ($menu_value['child'] as $submenu_value) {
                $format_menu[$submenu_value['action']] = array(
                    'model' => $key,
                    'model_name' => $menu_value['name'],
                    'name' => $submenu_value['name'],
                    'action' => $submenu_value['action'],
                    'op' => $submenu_value['op'],
                );
            }
        }
        return $format_menu;
    }

}