<?php
// +----------------------------------------------------------------------
// | ProjectName : domall
// +----------------------------------------------------------------------
// | Description :  商品管理
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2016 http://www.idowe.com All rights reserved.
// +----------------------------------------------------------------------
// | Authors : Johhny <chenjf@idowe.com>  Date : 2016-03-01
// +----------------------------------------------------------------------
namespace app\seller\controller;

class Goods extends Base{

    /**
     * 出售中的商品
     * @author Johhny <chenjf@idowe.com>
     * @return string
     */
    public function index(){
        $goods_list = M('goods')->where(array('goods_state'=>'1'))->select();
        $this->assign('goods_list',$goods_list);
        return $this->display();
    }

    /**
     * 仓库中的商品
     * @author Johhny <chenjf@idowe.com>
     */
    public function warehouse(){
        $goods_list = M('goods')->where(array('goods_state'=>'0'))->select();
        $this->assign('goods_list',$goods_list);
        return $this->display();
    }

    /**
     * 第一步：商品分类选择
     * @author Johhny <chenjf@idowe.com>
     * @return string
     * @throws \think\Exception
     */
    public function add(){
        if(IS_POST){
            $data = $_POST;
            $store_info = M('store')->where(array('store_id'=>$this->seller_info['store_id']))->find();
            $data['store_id'] = $this->seller_info['store_id'];
            $data['store_name'] = $store_info['store_name'];
            $data['gc_id_1'] = 0;
            $data['gc_id_2'] = 0;
            $data['gc_id_3'] = 0;
            if(!$data['gc_id']){
                $this->error('请选择分类');
            }
            $goods_class = M('goods_class')->where(array('gc_id'=>$data['gc_id']))->find();
            $data['gc_name'] = $goods_class['gc_name'];
            $data['goods_promotion_price'] = $data['goods_marketprice'];
            $data['goods_image'] = $data['goods_image'];
            $data['goods_discount'] = 0;
            $data['goods_storage_alarm'] = 0;
            $data['spec_name'] = '';
            $data['spec_value'] = '';
            $data['goods_spec'] = '';
            $data['goods_attr'] = '';
            $data['goods_custom'] = '';
            $data['mobile_body'] = '';
            $data['goods_state'] = '1';
            $data['goods_verify'] = '1';
            $data['goods_addtime'] = time();
            $data['goods_edittime'] = time();
            $data['goods_selltime'] = time();
            $data['areaid_1'] = '0';
            $data['areaid_2'] = '0';
            $data['transport_id'] = '0';
            $data['virtual_indate'] = time();
            $data['virtual_limit'] = '0';
            $data['sup_id'] = '0';
            M('goods')->add($data);
            return $this->success('添加成功','/seller/goods');
        }else {
            //商品分类
            $goods_class = M('goods_class')->where(array('gc_parent_id' => 0))->select();

            $this->assign('goods_class', $goods_class);

            return $this->display();
        }
    }
}