<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
// $Id$

return [
    'version'=>'1.0.0', //版本号
    'product_name' => 'DoMall', //产品名称
    'index_theme'=>'wap',//前台主题
    'index_view_suffix'=>'.html',
    'index_view_depr'=>'_',
    'seller_theme'=>'seller',//商家后台主题
    'seller_view_suffix'=>'.html',
    'seller_view_depr'=>'_',
    'admin_theme'=>'admin',//后台主题
    'admin_view_suffix'=>'.html',
    'admin_view_depr'=>'_',
    'admin_templates' => '/templates/admin',
    'index_templates' => '/templates/index',
    'seller_templates' => '/templates/seller',
    'web_url'=>'http://domall.idowe.com',
    'url_route_on' => true,
    'FILE_UPLOAD_TYPE'      =>  'Local',    // 文件上传方式
//    'log'          => [
//        'type'             => 'trace', // 支持 socket trace file
//        // 以下为socket类型配置
//        'host'             => '111.202.76.133',
//        //日志强制记录到配置的client_id
//        'force_client_ids' => [],
//        //限制允许读取日志的client_id
//        'allow_client_ids' => [],
//    ],
    'var_page' => 'p',
    'admin_page_rows' => 10,
];
