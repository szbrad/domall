<?php
namespace app\index\controller;

use app\common\model\Goods;

class Index extends Base
{
    /**2016-03-13
     * 未登录情况下输网址进不了商城首页
     * @author Roman Liu
     */
    public function index(){
        $goods_list = Goods::all();
        $this->assign('goods_list',$goods_list);
        return $this->display();
    }
}