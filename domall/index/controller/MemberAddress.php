<?php
// +----------------------------------------------------------------------
// | ProjectName : domall
// +----------------------------------------------------------------------
// | Description :  收货地址
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2016 http://www.idowe.com All rights reserved.
// +----------------------------------------------------------------------
// | Authors : Johhny <chenjf@idowe.com>  Date : 2016-03-03
// +----------------------------------------------------------------------
namespace app\index\controller;

class MemberAddress extends Base
{
    public function index(){
        $address_list = M('address')->where(array('member_id'=>$this->user_info['member_id']))->select();
        $this->assign('address_list',$address_list);
        return $this->display();
    }

    /**
     * 保存收货地址
     * @author Johhny <chenjf@idowe.com>
     */
    public function save(){
        if(IS_POST){
            if($_POST['is_default'] == 1){
                $where['member_id'] = $this->user_info['member_id'];
                $where['is_default'] = 1;
                $address_info = M('address')->where($where)->find();
                if($address_info){
                    $data['is_default']=0;
                    $data['address_id'] = $address_info['address_id'];
                    M('address')->save($data);
                }
            }
            $_POST['member_id'] = $this->user_info['member_id'];
            M('address')->add($_POST);

            return $this->success('保存成功','/index/member_address');
        }
    }
}