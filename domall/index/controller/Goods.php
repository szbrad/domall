<?php
// +----------------------------------------------------------------------
// | ProjectName : domall
// +----------------------------------------------------------------------
// | Description :  商品信息
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2016 http://www.idowe.com All rights reserved.
// +----------------------------------------------------------------------
// | Authors : Johhny <chenjf@idowe.com>  Date : 2016-03-02
// +----------------------------------------------------------------------

namespace app\index\controller;

class Goods extends Base
{
    /**
     * 产品详情
     * @author Johhny <chenjf@idowe.com>
     * @return string
     * @throws \think\Exception
     */
    public function index(){
        $id = $_GET['id'];
        $goods_info = M('goods')->where(array('goods_id'=>$id))->find();

        $where['buyer_id'] = $this->user_info['member_id'];
        $cart_info = M('cart')->where($where)->select();
        $cart_info_price = 0;
        foreach($cart_info as $item){
            $cart_info_price = $cart_info_price+ $item['goods_num']*$item['goods_price'];
        }

        $this->assign('cart_info_price',$cart_info_price);
        $this->assign('goods_info',$goods_info);
        return $this->display();
    }
}
