<?php
// +----------------------------------------------------------------------
// | ProjectName : domall
// +----------------------------------------------------------------------
// | Description :  订单
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2016 http://www.idowe.com All rights reserved.
// +----------------------------------------------------------------------
// | Authors : Johhny <chenjf@idowe.com>  Date : 2016-03-03
// +----------------------------------------------------------------------
namespace app\index\controller;

class Order extends Base
{
    public function index(){
        $address_where['member_id'] = $this->user_info['member_id'];
        $address_where['is_default'] = '1';
        $address_info = M('address')->where($address_where)->find();
        if(!$address_info){
            $address_info = M('address')->where(array('member_id'=>$this->user_info['member_id']))->order('address_id desc')->find();
        }

        //购物车列表
        $where['buyer_id'] = $this->user_info['member_id'];
        $cart_list = M('cart')->where($where)->select();
        $this->assign('cart_list',$cart_list);
        $this->assign('address_info',$address_info);
        return $this->display();
    }

    /**
     * 提交订单
     * @author Johhny <chenjf@idowe.com>
     */
    public function save(){
        if(IS_POST){

            $order =  D('orders')->saveOrder($this->user_info);

            if(!isset($order['error'])) {
                return $this->success('保存成功','/index/pay?paysn='.$order);
            }else{
                return $this->error($order['error']);
            }

        }
    }

    /**
     * 订单列表
     * @author Johhny <chenjf@idowe.com>
     */
    public function orderlist(){
        $order_list = M('orders')->where(array('buyer_id'=>$this->user_info['member_id']))->select();
        $this->assign('order_list',$order_list);
        return $this->display();
    }
}