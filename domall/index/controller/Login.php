<?php
// +----------------------------------------------------------------------
// | ProjectName : domall
// +----------------------------------------------------------------------
// | Description :  前台用户登录
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2016 http://www.idowe.com All rights reserved.
// +----------------------------------------------------------------------
// | Authors : Johhny <chenjf@idowe.com>  Date : 2016-02-24
// +----------------------------------------------------------------------
namespace app\index\controller;


use app\common\model\Member;
use think\Request;

class Login extends Base{

    /**
     * 前台登录
     * @author Johhny <chenjf@idowe.com>
     */
    public function index(Request $request){
        if($request->isPost()){
            $username = $_POST['username'];
            $password = $_POST['password'];

            $array = array();
            $array['member_name']   = $username;
            $array['member_passwd'] = md5($password);
            $member_info = Member::where($array)->find();

            if($member_info){

                $this->memberSetKey($member_info);

                $this->redirect('/index/index');
            }else{
                return $this->error('用户名或密码错误');
            }
        }else{
            return $this->fetch();
        }
    }

    /**
     * 前台注册
     * @author Johhny <chenjf@idowe.com>
     */
    public function register(Request $request){
        if($request->isPost()){
            $register_info = array();
            $register_info['username'] = $_POST['username'];
            $register_info['password'] = $_POST['password'];
            $register_info['email'] = $_POST['username'];
            $member_info = D('member')->register($register_info);
            if(!isset($member_info['error'])) {
                $array['member_name']   = $_POST['username'];
                $array['member_passwd'] = md5($_POST['password']);
                $member_info = M('member')->where($array)->find();

                if($member_info){

                    $this->memberSetKey($member_info);

                    $this->redirect('/index/index');
                }else{
                    return $this->error('用户名或密码错误');
                }
            } else {
                return $this->error($member_info['error']);
            }
        }else{
            return $this->display('register');
        }

    }

    /**
     * 用户退出
     * @author Johhny <chenjf@idowe.com>
     */
    public function logout(){
        session(null);
        return $this->redirect('/index/login');
    }
}