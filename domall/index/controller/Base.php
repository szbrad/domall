<?php
// +----------------------------------------------------------------------
// | ProjectName : domall
// +----------------------------------------------------------------------
// | Description :  首页
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2016 http://www.idowe.com All rights reserved.
// +----------------------------------------------------------------------
// | Authors : Johhny <chenjf@idowe.com>  Date : 2016-02-05
// +----------------------------------------------------------------------
namespace app\index\controller;

use think\Controller;
use think\Request;
use think\View;

class Base extends Controller{

    /**
     * 用户信息
     */
    protected $user_info;

    /**
     * 构造函数
     * Base constructor.
     */
    public function __construct() {
        parent::__construct();

        /**
         * 验证用户是否登录
         * $user_info 用户资料
         */
        $this->user_info = $this->userLogin();
        $this->assign('user_info',$this->user_info);
    }

    /**
     * 系统后台登录验证，并判断是否已经登录，
     * 登录后则自动跳转平台后台首页
     * @param
     * @return array 数组类型的返回结果
     */
    protected final function userLogin() {
        $user = unserialize(session('user_key'));
        if (empty($user)){
            if($this->request->controller() != 'Login'){
                $this->redirect('/index/login');
            }
        }else {
            if($this->request->controller() == 'login'){
                $this->redirect('/index/index');
            }
        }
        return $user;
    }

    /**
     * 重写fetch 方便管理视图 主题风格
     * @author Johhny <chenjf@idowe.com>
     * @param string $template
     * @param array $vars
     * @param array $cache
     * @param bool $renderContent
     * @return string
     * @throws \think\Exception
     */
    public function display($template = '', $vars = [], $replace = [], $config = []){

        $this->view->engine([
            'view_path'     => APP_PATH.'/index/view/'.config('index_theme').'/',
            'view_suffix'   => config('index_view_suffix'),
            'view_depr'     => config('index_view_depr'),
        ]);
        return parent::fetch($template,$vars,$replace,$config);
    }

    /**
     * 重写fetch 方便管理视图 主题风格
     * @author Johhny <chenjf@idowe.com>
     * @param string $template
     * @param array $vars
     * @param array $cache
     * @param bool $renderContent
     * @return string
     * @throws \think\Exception
     */
    public function fetch($template = '', $vars = [], $cache = [], $renderContent = false){
        return $this->display($template,$vars,$cache,$renderContent);
    }

    /**
     * 用户后台 用户登录后 将会员验证内容写入对应cookie中
     *
     * @param string $seller 商户信息
     */
    protected final function memberSetKey($member) {
        session('user_key',serialize($member));
    }
}