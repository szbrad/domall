<?php
// +----------------------------------------------------------------------
// | ProjectName : domall
// +----------------------------------------------------------------------
// | Description :  用户模型
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2016 http://www.idowe.com All rights reserved.
// +----------------------------------------------------------------------
// | Authors : Johhny <chenjf@idowe.com>  Date : 2016-02-25
// +----------------------------------------------------------------------
namespace app\common\model;
use think\Exception;
use think\Model;
class Member extends Common{

    /**
     * 注册
     */
    public function register($register_info) {


        // 验证用户名是否重复
        $check_member_name  = $this->where(array('member_name'=>$register_info['username']))->find();
        if(is_array($check_member_name) and count($check_member_name) > 0) {
            return array('error' => '用户名已存在');
        }

        // 验证邮箱是否重复
        $check_member_email = $this->where(array('member_email'=>$register_info['email']))->find();
        if(is_array($check_member_email) and count($check_member_email)>0) {
            return array('error' => '邮箱已存在');
        }
        // 会员添加
        $member_info    = array();
        $member_info['member_name']     = $register_info['username'];
        $member_info['member_passwd']   = $register_info['password'];
        $member_info['member_email']        = $register_info['email'];
//        $member_info['invite_one']        = $register_info['invite_one'];
//        $member_info['invite_two']        = $register_info['invite_two'];
//        $member_info['invite_three']      = $register_info['invite_three'];
        $insert_id  = $this->addMember($member_info);
        if($insert_id) {
            $member_info['member_id'] = $insert_id;
            $member_info['is_buy'] = 1;

            return $member_info;
        } else {
            return array('error' => '注册失败');
        }

    }

    /**
     * 注册商城会员
     *
     * @param   array $param 会员信息
     * @return  array 数组格式的返回结果
     */
    public function addMember($param) {
        if(empty($param)) {
            return false;
        }
        try {
            $this->db->startTrans();
            $member_info    = array();
            if(isset($param['member_id'])){
                $member_info['member_id']           = $param['member_id'];
            }

            $member_info['member_name']         = $param['member_name'];
            $member_info['member_passwd']       = md5(trim($param['member_passwd']));
            $member_info['member_email']        = $param['member_email'];
            $member_info['member_time']         = time();
            $member_info['member_login_time']   = time();
            $member_info['member_old_login_time'] = time();
            $member_info['member_login_ip']     = $this->getIp();
            $member_info['member_old_login_ip'] = $member_info['member_login_ip'];

            if(isset($param['member_truename'])){
                $member_info['member_truename']     = $param['member_truename'];
            }
            if(isset($param['member_qq'])) {
                $member_info['member_qq'] = $param['member_qq'];
            }
            if(isset($param['member_sex'])) {
                $member_info['member_sex'] = $param['member_sex'];
            }
            if(isset($param['member_avatar'])) {
                $member_info['member_avatar'] = $param['member_avatar'];
            }
            if(isset($param['member_qqopenid'])) {
                $member_info['member_qqopenid'] = $param['member_qqopenid'];
            }
            if(isset($param['member_qqinfo'])) {
                $member_info['member_qqinfo'] = $param['member_qqinfo'];
            }
            if(isset($param['member_sinaopenid'])) {
                $member_info['member_sinaopenid'] = $param['member_sinaopenid'];
            }
            if(isset($param['member_sinainfo'])) {
                $member_info['member_sinainfo'] = $param['member_sinainfo'];
            }
            if(isset($param['invite_one'])) {
                $member_info['invite_one'] = $param['invite_one'];
            }
            if(isset($param['invite_two'])) {
                $member_info['invite_two'] = $param['invite_two'];
            }
            if(isset($param['invite_three'])) {
                $member_info['invite_three'] = $param['invite_three'];
            }
            if (isset($param['member_mobile_bind'])) {
                if (isset($param['member_mobile'])) {
                    $member_info['member_mobile'] = $param['member_mobile'];
                }
                $member_info['member_mobile_bind'] = $param['member_mobile_bind'];
            }
            if (isset($param['weixin_unionid'])) {
                $member_info['weixin_unionid'] = $param['weixin_unionid'];
                if (isset($param['weixin_info'])) {
                    $member_info['weixin_info'] = $param['weixin_info'];
                }
            }
            $insert_id  = $this->add($member_info);
            if (!$insert_id) {
                E('注册会员失败');
            }

            $this->db->commit();
            return $insert_id;
        } catch (Exception $e) {
            $this->db->rollback();
            return false;
        }
    }

    /**
     * 取得IP
     *
     *
     * @return string 字符串类型的返回结果
     */
    function getIp(){
        $ip = $_SERVER['REMOTE_ADDR'];
        return preg_match('/^\d[\d.]+\d$/', $ip) ? $ip : '';
    }
}