<?php
// +----------------------------------------------------------------------
// | ProjectName : domall
// +----------------------------------------------------------------------
// | Description :  店铺模型
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2016 http://www.idowe.com All rights reserved.
// +----------------------------------------------------------------------
// | Authors : Johhny <chenjf@idowe.com>  Date : 2016-02-19
// +----------------------------------------------------------------------
namespace app\common\model;
use think\Model;
class Store extends Common{

    /**
     * 通过店铺编号查询店铺信息
     *
     * @param int $store_id 店铺编号
     * @return array
     */
    public function getStoreInfoByID($store_id) {
        $prefix = 'store_info';

        //读取缓存
        $store_info = S($prefix.$store_id);
        if(empty($store_info)) {
            $store_info = $this->getStoreInfo(array('store_id' => $store_id));
            $cache = array();
            $cache['store_info'] = serialize($store_info);
            //写入缓存
            S($prefix.$store_id,$cache,3600);
        } else {
            $store_info = unserialize($store_info['store_info']);
        }
        return $store_info;
    }

    /**
     * 查询店铺信息
     *
     * @param array $condition 查询条件
     * @return array
     */
    public function getStoreInfo($condition) {
        $store_info = $this->where($condition)->find();
        if(!empty($store_info)) {
            if(!empty($store_info['store_presales'])) $store_info['store_presales'] = unserialize($store_info['store_presales']);
            if(!empty($store_info['store_aftersales'])) $store_info['store_aftersales'] = unserialize($store_info['store_aftersales']);

            //商品数
            $store_info['goods_count'] = D('goods')->getGoodsCommonOnlineCount(array('store_id' => $store_info['store_id']));

            //店铺评价
//            $model_evaluate_store = Model('evaluate_store');
//            $store_evaluate_info = $model_evaluate_store->getEvaluateStoreInfoByStoreID($store_info['store_id'], $store_info['sc_id']);

//            $store_info = array_merge($store_info, $store_evaluate_info);
            $store_info = array_merge($store_info);
        }
        return $store_info;
    }
}