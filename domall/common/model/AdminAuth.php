<?php
/**
 * SCRIPT_NAME: AdminRule.php
 * Created by PhpStorm.
 * Time: 2016/3/18 21:25
 * FUNCTION:
 * @author: Doogie <461960962>
 */

namespace app\common\model;

class AdminAuth extends Common {

    /**
     * 类型
     * @param null $type
     * @return array
     */
    public function types($type=null){
        $list = [
            1 => '菜单',
            2 => '权限'
        ];
        return isset($list[$type]) ? $list[$type] : $list;
    }

    /**
     * 最多三层菜单
     * @param $role_info
     * @param $admin_info
     * @return false|\PDOStatement|string|\think\Collection
     */
    public function getMenus($role_info,$admin_info){
        $map = ['status' => 1, 'pid' => '0', 'type' => 1];
        $l1 = $this->where($map)->order('sort asc')->select();
        $role_group = unserialize($role_info['role_group']);
//        if(!$role_group)return $l1;
        if($l1){
            foreach($l1 as &$vo){
                if($admin_info['admin_is_super'] == 1){
                    $map['pid'] = $vo['id'];
                    $l2 = $this->where($map)->select();
                    if($l2){
                        foreach($l2 as &$v){
                            $map['pid'] = $v['id'];
                            $l3 = $this->where($map)->select();
                            $l3 && $v['_child'] = $l3;
                        }
                        $vo['_child'] = $l2;
                    }
                }else{
                    $map['pid'] = $vo['id'];
                    $l2 = $this->where('pid','=',$vo['id'])
                        ->where(function($query) use ($role_group) {
                            $query->where('pid','in',$role_group)
                                ->where('id','in',$role_group);
                        })
                        ->where('status=1 and type =1')
                        ->select();
                    if($l2){
                        foreach($l2 as &$v){
                            $map['pid'] = $v['id'];
                            $l3 = $this->where('pid','=',$v['id'])
                                ->where(function($query) use ($role_group) {
                                    $query->where('pid','in',$role_group)
                                        ->where('id','in',$role_group);
                                })
                                ->where('status=1 and type =1')
                                ->select();
                            $l3 && $v['_child'] = $l3;
                        }
                        $vo['_child'] = $l2;
                    }
                }
            }
        }
        return $l1;
    }

    /**
     * 获取当前菜单
     * @return mixed
     */
    public function getCurrentMenu(){
        $url = request()->controller() . '/' . request()->action();

        return $this->where(['url' => ['like', '%'.$url.'%'], 'status' => 1, 'type'=>1])->find();
    }

    /*public function getMenuTree(&$tree, $pid=0){
        $condition = ['pid' => $pid, 'status'=>1];
        if($result = $this->where($condition)->order('sort asc')->select()){
            foreach($result as $vo){
                $tree[$vo['id']] = $vo;
                $this->getMenuTree($tree, $vo['id']);
            }
            return true;
        }else
            return false;
    }*/
}