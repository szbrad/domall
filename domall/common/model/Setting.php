<?php
/**
 * SCRIPT_NAME: Setting.php
 * Created by PhpStorm.
 * Time: 2016/3/16 23:25
 * FUNCTION:
 * @author: Doogie
 */

namespace app\common\model;

use think\Model;
class Setting extends Common {
    /**
     * 配置类型
     * @return array
     */
    public function types()
    {
        $config = $this->where(['name'=>'config_type_list'])->find();
        if ($config) {
            $types = parse($config['type'], $config['value']);
        } else {
            $types = [
                'num' => '数字',
                'text' => '文本',
                'array' => '数组',
                'enum' => '枚举'
            ];
        }
        return $types;
    }

    /**
     * 配置分组
     * @return array
     */
    public function groups()
    {
        $group = $this->where(['name'=>'config_group_list'])->find();
        if ($group['value']) {
            $groups = parse($group['type'], $group['value']);
        } else {
            $groups = [0 =>'全部', 1=>'基本配置'];
        }
        return $groups;
    }
}