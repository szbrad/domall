<?php
// +----------------------------------------------------------------------
// | ProjectName : domall
// +----------------------------------------------------------------------
// | Description :  管理表
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2016 http://www.idowe.com All rights reserved.
// +----------------------------------------------------------------------
// | Authors : Johhny <chenjf@idowe.com>  Date : 2016-05-21
// +----------------------------------------------------------------------
namespace app\common\model;
class Admin extends Common {

    //  生成运营中心唯一编号
    public function getIdentifying(){
        $isIdentify     =   $this->max('identifying');
        $identifyLen    =   strlen($isIdentify);
        $isIdentify +=1;

        if( $identifyLen <=  3 ){
            $zero   =   "";
            for($i = 1; $i<= 3-$identifyLen ; $i++){
                $zero.="0";
            }
            $isIdentify =   $zero.($isIdentify);
        }

        return $isIdentify;
    }
}